# Intake platform

Платформа для набора участников.

# Env

| Переменная | Назначение |
| ------------- | ------------- |
| MAILGUN_API_KEY | Api key от mailgun |
| ROBOT_PASSWORD | Пароль от аккаунта робота |
| SECRET_KEY | секретный ключ |
| SENTRY_DSN | dsn от sentry |
| TG_BOT_TOKEN | токен от телеграмовского боты |
| DATABASE_URL | url к postgresql |

# Dev

Должен стоять docker, docker-compose и poetry

Установка зависимостей:

```bash
$ poetry install
```

Инициализация рабочего окружения

```bash
$ make init_env
```

Команда создаст `.env` файл и запустит внешние сервисы-зависимости.
В файл надо вписать нормальные значения.

Запуск:

```bash
$ make django-run
```

Доп команды:

Выключение окружения

```bash
$ make stop_env
```

Запуск окружения по новой

```bash
$ make start_env
```

Удаление окружения и всех данных

```bash
$ make rm_env
```
