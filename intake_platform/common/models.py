from datetime import datetime

from django.db import models
from django.contrib.auth.models import User

from django.conf import settings
from intake_platform.intake.tools import get_wp_passed_apps
from intake_platform.intake.models import ApplicationCase


class Location(models.Model):
    city = models.CharField(u"Город", max_length=200)
    region = models.CharField(u"Область", max_length=200)
    district = models.CharField(u"Округ", max_length=200)

    def __str__(self):
        return u"%s, %s, %s" % (self.city, self.region, self.district)


class UserRestrictions(models.Model):
    RESTRICT_CURATOR_READONLY = 1

    RESTRICTION_TYPE_CHOICES = [
        (RESTRICT_CURATOR_READONLY, u"Ограничить функции куратору: только READ")
    ]

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    restriction_type = models.IntegerField(
        u"Тип ограничения",
        choices=RESTRICTION_TYPE_CHOICES,
        default=RESTRICT_CURATOR_READONLY,
    )

    def __str__(self):
        return u"Пользователь <%s>: %s" % (
            self.user.email,
            self.get_restriction_type_display(),
        )

    class Meta:
        app_label = "common"
        verbose_name = u"Ограничение пользователя"
        verbose_name_plural = u"Ограничения пользователей"


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    middle_name = models.CharField(u"Отчество", max_length=100, null=True)
    is_male = models.BooleanField(u"Этот участник мужского пола", default=False)
    location = models.CharField(
        u"Место постоянного проживания", max_length=500, null=True
    )
    birthdate = models.DateField(null=True)

    # TODO: UserBuck? Roles? This bools array is not very convenient
    is_nabor = models.BooleanField(u"Этот участник из группы набора", default=False)
    is_curator = models.BooleanField(u"Это куратор какой-то мастерской", default=False)
    is_guru = models.BooleanField(u"Это гуру", default=False)

    is_beta_tester = models.BooleanField(
        u"Бета-тестер",
        help_text=u"Есть доступ к новым фичам на сайте",
        default=False,
    )

    def has_all_basic_fields(self):
        return all(
            field is not None
            for field in (
                self.first_name,
                self.last_name,
                self.middle_name,
                self.location,
                self.age
            )
        )

    @property
    def is_castrated(self):
        if not self.is_curator:
            return False
        return UserRestrictions.objects.filter(
            restriction_type=UserRestrictions.RESTRICT_CURATOR_READONLY, user=self.user
        ).exists()

    @property
    def is_restricted(self):
        return UserRestrictions.objects.filter(user=self.user).exists()

    @property
    def restrictions(self):
        return UserRestrictions.objects.filter(user=self.user)

    @property
    def is_adequate(self):
        return self.is_beta_tester

    def __str__(self):
        return u"%s (%s)" % (self.user, self.user.email)

    @property
    def first_name(self):
        return self.user.first_name

    @first_name.setter
    def first_name(self, first_name):
        self.user.first_name = first_name
        self.user.save()

    @property
    def last_name(self):
        return self.user.last_name

    @last_name.setter
    def last_name(self, last_name):
        self.user.last_name = last_name
        self.user.save()

    @property
    def email(self):
        return self.user.email

    @property
    def is_underage(self):
        return self.age < 18

    def age_on_date(self, date):
        """
            Return age in full years when the date arrives.

            Returns -1 if the date argument is before self.birthdate

            :arg date: either a datetime object or a tuple to instance it
        """
        if self.birthdate is None:
            return -1
        if isinstance(date, datetime):
            t_diff = date.date() - self.birthdate
        elif isinstance(date, tuple):
            t_diff = datetime(*date).date() - self.birthdate
        return t_diff.days / 365 if t_diff.days > 0 else -1

    @property
    def is_adult(self):
        start_tuple = settings.LSH_DATES_DICT["start"]
        age_on_start = self.age_on_date(start_tuple)
        return age_on_start >= 18

    @property
    def is_female(self):
        return not self.is_male

    @property
    def age(self) -> int:
        return int(self.age_on_date(datetime.now()))

    def workshop_programs(self, include_apps=False):
        if not self.is_curator:
            return []

        programs = self.user.workshopprogrammetainfo_set.all()
        if include_apps:
            # FIXME: the apps part needs to be rearranged
            return {
                u"{}-{}".format(x.workshop_slug, x.program_slug): {
                    "meta": x.as_dict(),
                    "apps": get_wp_passed_apps(x.workshop_slug, x.program_slug),
                }
                for x in programs
            }
        else:
            return {
                u"{}-{}".format(x.workshop_slug, x.program_slug): x.as_dict()
                for x in programs
            }

    def is_curator_for(self, w_slug, p_slug):
        if not self.is_curator:
            return False

        w_metas = self.user.workshopprogrammetainfo_set.all()
        for w_m in w_metas:
            if w_m.workshop_slug == w_slug and w_m.program_slug == p_slug:
                return True
        return False

    def is_curator_for_app(self, pk):
        if not self.is_curator:
            return False

        case = ApplicationCase.objects.get(pk=pk)

        w_metas = self.user.workshopprogrammetainfo_set.all()
        w_slug = case.short_info()["wpc"]["workshop_slug"]
        p_slug = case.short_info()["wpc"]["program_slug"]
        for w_m in w_metas:
            if w_m.workshop_slug == w_slug and w_m.program_slug == p_slug:
                return True
        return False

    def has_goodies(self):
        return any([self.is_curator, self.is_nabor, self.is_guru])
