from django import forms
from django.forms import widgets
from selectable.forms import AutoCompleteWidget

from .lookups import LocationLookup
from .models import UserProfile

# TODO: Пол неправильно сохраняется почему-то

GENDER_CHOICES = [(True, u"Мужской"), (False, u"Женский")]


class LocationForm(forms.Form):
    city = forms.CharField(
        label=u"Город", widget=AutoCompleteWidget(LocationLookup), required=True
    )


class MySignupForm(forms.Form):
    last_name = forms.CharField(label=u"Фамилия", max_length=100)
    first_name = forms.CharField(label=u"Имя", max_length=100)
    middle_name = forms.CharField(label=u"Отчество", max_length=100)
    is_male = forms.TypedChoiceField(
        label=u"Пол",
        coerce=lambda x: x.startswith(u"True"),  # lol
        choices=GENDER_CHOICES,
    )
    location = forms.CharField(
        label=u"Город",
        widget=AutoCompleteWidget(LocationLookup, allow_new=True),
        max_length=400,
        required=True,
    )
    birthdate = forms.DateField(
        label=u"Дата рождения",
        widget=widgets.SelectDateWidget(years=range(2005, 1950, -1)),
    )

    email = forms.EmailField(label=u"e-mail")

    def signup(self, request, user):
        cd = self.cleaned_data
        user.first_name = cd["first_name"]
        user.last_name = cd["last_name"]
        user.email = cd["email"]
        user.save()

        p = UserProfile(
            user=user,
            is_beta_tester=False,
            is_curator=user.email.endswith("@letnyayashkola.org"),
        )
        p.middle_name = cd["middle_name"]
        p.is_male = cd["is_male"]
        p.location = cd["location"]
        p.birthdate = cd["birthdate"]
        p.save()
