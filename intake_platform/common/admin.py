from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from .models import Location, UserProfile, UserRestrictions


class UserProfileInline(admin.StackedInline):
    model = UserProfile
    can_delete = False


class MyUserAdmin(UserAdmin):
    inlines = (UserProfileInline,)


admin.site.register(UserRestrictions)
admin.site.register(Location)
admin.site.unregister(User)
admin.site.register(User, MyUserAdmin)
