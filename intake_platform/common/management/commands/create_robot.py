from django.core.management.base import BaseCommand

from django.contrib.auth import get_user_model
from django.conf import settings
from intake_platform.common.models import UserProfile


class Command(BaseCommand):
    help = "Create robot account if not exists"

    def handle(self, *args, **options):
        model = get_user_model()

        first, last = settings.ROBOT_USERNAME.split(".")

        user, created = model.objects.get_or_create(
            username=settings.ROBOT_USERNAME,
            first_name=first,
            last_name=last,
            email=f"{settings.ROBOT_USERNAME}@letnyayashkola.org",
            password=settings.ROBOT_PASSWORD,
        )

        if not created:
            print("No need to create user for robot, as it already exists")
            return

        UserProfile.objects.create(user=user, is_male=False)
        print("Success")
