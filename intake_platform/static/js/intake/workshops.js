function WorkshopProgram(data) {
  this.workshop_slug = ko.observable(data.workshop_slug);
  this.program_slug = ko.observable(data.program_slug);
  this.workshop_name = ko.observable(data.workshop_name);
  this.program_name = ko.observable(data.program_name);

  this.wp_name = ko.computed(function() {
    return this.workshop_name() + " --> " + this.program_name();
  }, this);

  this.wp_data = ko.computed(function() {
    return this.workshop_slug() + ":" + this.program_slug();
  }, this);
}

function WorkshopProgramsViewModel() {
  var self = this;
  self.workshop_programs = ko.observableArray([]);
  self.selected_wp = ko.observable();
  //
  // fetch emails
  $.getJSON("/workshop-programs", function(allData) {
    mappedData = $.map(allData, function(item) { return new WorkshopProgram(item) });
    self.workshop_programs(mappedData);
  });
  console.log('WPVM loaded');
}

ko.applyBindings(new WorkshopProgramsViewModel());
