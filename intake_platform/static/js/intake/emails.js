function Email(data) {
  this.date = ko.observable(data.date);
  this.subject = ko.observable(data.subject);
  this.text = ko.observable(data.text);
  this.buck = ko.observable(data.buck);
}

function EmailsViewModel() {
  var self = this;
  self.emails = ko.observableArray([]);
  //
  // fetch emails
  $.getJSON("/emails", function(allData) {
    mappedEmails = $.map(allData, function(item) { return new Email(item) });
    self.emails(mappedEmails);
  });
}

ko.applyBindings(new EmailsViewModel());
