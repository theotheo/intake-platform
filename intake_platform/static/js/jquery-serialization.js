(function($) {
  $.fn.extend({
    toObject: function() {
      var result = {};
      $.each(this.serializeArray(), function(i, v) {
        result[v.name] = v.value;
      });
      return result;
    },
  });
})(jQuery);
