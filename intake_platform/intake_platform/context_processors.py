from datetime import datetime

from django.conf import settings


def now(requset):
    now = datetime.now()
    return {"now": now}


def version(_):
    return {"version": settings.VERSION}
