curators_dict = {
    # andan
    u"a.fenin@letnyayashkola.org": {"workshop_slug": u"andan"},
    u"a.zhukova@letnyayasnkola.org": {"workshop_slug": u"andan"},
    u"m.servetnik@letnyayashkola.org": {"workshop_slug": u"andan"},
    u"e.rybina@letnyayashkola.org": {"workshop_slug": u"andan"},
    u"g.moroz@letnyayashkola.org": {"workshop_slug": u"andan"},
    u"e.kozhanova@letnyayashkola.org": {"workshop_slug": u"andan"},
    # astrogeo
    u"k.vlasov@letnyayashkola.org": {"workshop_slug": u"astrogeo"},
    u"margo-kira@letnyayashkola.org": {"workshop_slug": u"astrogeo"},
    u"ehllatypova@letnyayashkola.org": {"workshop_slug": u"astrogeo"},
    u"a.ivanova@letnyayashkola.org": {"workshop_slug": u"astrogeo"},
    u"n.nedelko@letnyayashkola.org": {"workshop_slug": u"astrogeo"},
    # kind
    u"e.korovkina@letnyayashkola.org": {"workshop_slug": u"kind"},
    u"mari_antonoffa@letnyayashkola.org": {"workshop_slug": u"kind"},
    u"sergey.afanasiev@letnyayashkola.org": {"workshop_slug": u"kind"},
    u"garik@letnyayashkola.org": {"workshop_slug": u"kind"},
    # dok-kino
    u"m.glebova@letnyayashkola.org": {"workshop_slug": u"dok-kino"},
    u"i.vlasova@letnyayashkola.org": {"workshop_slug": u"dok-kino"},
    u"d.krylova@letnyayashkola.org": {"workshop_slug": u"dok-kino"},
    u"aborisenko@letnyayashkola.org": {"workshop_slug": u"dok-kino"},
    u"helga@letnyayashkola.org": {"workshop_slug": u"dok-kino"},
    u"polyakoval@mediamarkt.ru": {"workshop_slug": u"dok-kino"},
    # 105element
    u"a.suschevich@letnyayashkola.org": {"workshop_slug": u"105element"},
    u"m.pilipenko@letnyayashkola.org": {"workshop_slug": u"105element"},
    u"a.bogomolova@letnyayashkola.org": {"workshop_slug": u"105element"},
    u"d.klimanskiy@letnyayashkola.org": {"workshop_slug": u"105element"},
    # bio
    u"v.kopylova@letnyayashkola.org": {"workshop_slug": u"bio"},
    u"o.grechman@letnyayashkola.org": {"workshop_slug": u"bio"},
    u"e.astahova@letnyayashkola.org": {"workshop_slug": u"bio"},
    # gamedesign
    u"a.kolodochka@letnyayashkola.org": {"workshop_slug": u"gamedesign"},
    u"a.knyazev@letnyayashkola.org": {"workshop_slug": u"gamedesign"},
    u"o.evdokima@letnyayashkola.org": {"workshop_slug": u"gamedesign"},
    # zhivoyteatr
    u"v.fufaeva@letnyayashkola.org": {"workshop_slug": u"zhivoyteatr"},
    u"khrapov@letnyayashkola.o}": {"workshop_slug": u"zhivoyteatr"},
    u"sadkov@letnyayashkola.org": {"workshop_slug": u"zhivoyteatr"},
    # illustration
    u"a.haritonova@letnyayashkola.org": {"workshop_slug": u"animation"},
    # L10n
    u"s.romanov@letnyayashkola.org": {"workshop_slug": u"L10n"},
    # rationality
    u"e.zvereva@letnyayashkola.org": {"workshop_slug": u"rationality"},
    # khudkino
    u"e.volodchenko@letnyayashkola.org": {"workshop_slug": u"khudkino"},
    u"v.kustov@letnyayashkola.org": {"workshop_slug": u"khudkino"},
    u"d.zhanaydarov@letnyayashkola.org": {"workshop_slug": u"khudkino"},
    u"it_is_apple@mail.ru": {"workshop_slug": u"khudkino"},
    # medo
    u"svetlana.koryukova@letnyayashkola.org": {"workshop_slug": u"medo"},
    u"d.venidiktova@letnyayashkola.org": {"workshop_slug": u"medo"},
    u"a.lobanova@letnyayashkola.org": {"workshop_slug": u"medo"},
    u"a.mishenko@letnyayashkola.org": {"workshop_slug": u"medo"},
    u"rakintsev@letnyayashkola.org": {"workshop_slug": u"medo"},
    u"ivan.sherbakov@letnyayashkola.org": {"workshop_slug": u"medo"},
    # obrazovanie
    u"v.baraeva@letnyayashkola.org": {"workshop_slug": u"obrazovanie"},
    u"veronika.baraeva@letnyayashkola.org": {"workshop_slug": u"obrazovanie"},
    u"k.baranova@letnyayashkola.org": {"workshop_slug": u"obrazovanie"},
    u"m.bulanov@letnyayashkola.org": {"workshop_slug": u"obrazovanie"},
    u"v.kruglov@letnyayashkola.org": {"workshop_slug": u"obrazovanie"},
    u"julia.volodina@letnyayashkola.org": {"workshop_slug": u"obrazovanie"},
    u"v.gaidamaka@letnyayashkola.org": {"workshop_slug": u"obrazovanie"},
    u"e.gurbanov@letnyayashkola.org": {"workshop_slug": u"obrazovanie"},
    u"o.danilyuk@letnyayashkola.org": {"workshop_slug": u"obrazovanie"},
    u"a.dorozhkina@letnyayashkola.org": {"workshop_slug": u"obrazovanie"},
    u"l.arkashina@letnyayashkola.org": {"workshop_slug": u"obrazovanie"},
    u"ju.egorova@letnyayashkola.org": {"workshop_slug": u"obrazovanie"},
    u"a.ivanova2@letnyayashkola.org": {"workshop_slug": u"obrazovanie"},
    u"ju.krinichnaya@letnyayashkola.org": {"workshop_slug": u"obrazovanie"},
    u"i.kultygina@letnyayashkola.org": {"workshop_slug": u"obrazovanie"},
    u"d.mikov@letnyayashkola.org": {"workshop_slug": u"obrazovanie"},
    u"a.minasyan@letnyayashkola.org": {"workshop_slug": u"obrazovanie"},
    u"a.mikhailova@letnyayashkola.org": {"workshop_slug": u"obrazovanie"},
    u"mary.dehany@letnyayashkola.org": {"workshop_slug": u"obrazovanie"},
    u"a.ozol@letnyayashkola.org": {"workshop_slug": u"obrazovanie"},
    u"yuri.romanov@letnyayashkola.org": {"workshop_slug": u"obrazovanie"},
    u"n.savostikov@letnyayashkola.org": {"workshop_slug": u"obrazovanie"},
    u"e.savostikova@letnyayashkola.org": {"workshop_slug": u"obrazovanie"},
    u"a.svirina@letnyayashkola.org": {"workshop_slug": u"obrazovanie"},
    u"e.skiba@letnyayashkola.org": {"workshop_slug": u"obrazovanie"},
    u"o.skobina@letnyayashkola.org": {"workshop_slug": u"obrazovanie"},
    u"v.fedorov@letnyayashkola.org": {"workshop_slug": u"obrazovanie"},
    u"e.khromova@letnyayashkola.org": {"workshop_slug": u"obrazovanie"},
    u"n.shendrik@letnyayashkola.org": {"workshop_slug": u"obrazovanie"},
    u"v.vasilyev@letnyayashkola.org ": {"workshop_slug": u"obrazovanie"},
    # prisma
    u"k.grichanina@letnyayashkola.org": {"workshop_slug": u"prisma"},
    # psycho
    u"igor.grigoriev@letnyayashkola.org": {"workshop_slug": u"psycho"},
    u"svetlana.skarlosh@letnyayashkola.org": {"workshop_slug": u"psycho"},
    # random
    u"a.tyurina@letnyayashkola.org": {"workshop_slug": u"random"},
    u"g.osmak@letnyayashkola.org": {"workshop_slug": u"random"},
    u"n.kolodin@letnyayashkola.org": {"workshop_slug": u"random"},
    u"a.onuchin@letnyayashkola.org": {"workshop_slug": u"random"},
    u"d.kovalev@letnyayashkola.org": {"workshop_slug": u"random"},
    u"z.bobyleva@letnyayashkola.org": {"workshop_slug": u"random"},
    # dance
    u"a.papina@letnyayashkola.org": {"workshop_slug": u"dance"},
    # philosophy
    u"d.aronson@letnyayashkola.org": {"workshop_slug": u"philosophy"},
    # frc
    u"e.varshavere@letnyayashkola.org": {"workshop_slug": u"frc"},
    u"varshavere@gmail.com": {"workshop_slug": u"frc"},
    u"n.ivanova@letnyayashkola.org": {"workshop_slug": u"frc"},
    # sci-pub
    u"ivan.shunin@letnyayashkola.org": {"workshop_slug": u"sci-pub"},
    # evolve
    u"n.nelupenko@letnyayashkola.org": {"workshop_slug": u"evolve"},
    # itip-law
    u"itiplaw@letnyayashkola.org": {"workshop_slug": u"itip-law"},
    # deeplearning
    u"i.belialov@letnyayashkola.org": {"workshop_slug": u"deeplearning"},
    u"n.devyatova@letnyayashkola.org": {"workshop_slug": u"deeplearning"},
}
