# coding: utf-8

from annoying.decorators import render_to
from annoying.functions import get_object_or_None
from intake_platform.common.tools import get_user
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.urls import reverse
from django.db.models import Q
from django.shortcuts import get_object_or_404, redirect, render
from django.template import loader
from django.views import generic

from .mixins import NaborRequiredMixin
from .decorators import intake_restricted
from .forms import AppEmailForm, ApplicationCommentForm, WPChangeForm
from .models import (
    AppEmail,
    ApplicationCase,
    BlackListCategory,
    BlackListEntry,
    GiveBackInfo,
    PastFuturePresentInfo,
    UnderageUserInfo,
    UserActivityInfo,
    UserAdditionalContacts,
    UserBuck,
    UserEducationInfo,
    UserHandymanshipInfo,
    UserLogisticsInfo,
    UserPhoto,
    WorkshopPetitionConnection,
    WorkshopProgramEmailTemplate,
)
from .tools import (
    attach_buck,
    comment_on_case,
    detach_buck,
    filter_users_by_buck_slug,
    first_step_completed,
    get_buck_nice_name,
    get_fresh_apps_for_nabor,
    get_latest_buck,
    get_workshop_accepted_apps,
    get_workshop_declined_apps,
    get_workshop_programs,
    get_wp_accepted_apps,
    has_buck,
    has_buck_after_buck,
    lick_buck,
    prepare_second_round,
    robot_comment_on_case,
    send_mail_to_applicant,
    workshops_dict,
)


@login_required
@render_to("intake/staff/count.html")
def check_1st_step(request):
    """ Possibly, deprecated, but I'm not in the mood to check

        -- cra, 2015 may 31
    """
    request.GET.get["email"]
    users = User.objects.filter(email__icontains=request.GET.get["email"])
    for u in users:
        related_cls = [
            ("WorkshopPetitionConnection", WorkshopPetitionConnection),
            ("UserAdditionalContacts", UserAdditionalContacts),
            ("UserEducationInfo", UserEducationInfo),
            ("UserActivityInfo", UserActivityInfo),
            ("UserHandymanshipInfo", UserHandymanshipInfo),
            ("GiveBackInfo", GiveBackInfo),
            ("UserLogisticsInfo", UserLogisticsInfo),
            ("PastFuturePresentInfo", PastFuturePresentInfo),
            ("UserPhoto", UserPhoto),
        ]
        if u.userprofile.is_underage:
            related_cls.append(("UnderageUserInfo", UnderageUserInfo)),

        print("------")
        print("User: %s" % u)
        print("email: %s" % u.email)
        for name, c in related_cls:
            obj = get_object_or_None(c, user=u)
            print("[{}]:\n  {}".format(name, obj))
        print("First step completed? %s" % first_step_completed(u.userprofile))

    return {"form": "f"}


class FreshAppsView(NaborRequiredMixin, generic.ListView):
    template_name = "intake/staff/fresh_apps.html"
    context_object_name = "applications"
    paginate_by = 50

    def get_queryset(self):
        users = User.objects.filter(userprofile__is_nabor=False)
        apps = get_fresh_apps_for_nabor(users)
        qs = ApplicationCase.objects.filter(
            user__in=apps, is_closed=False, is_suspicious=False
        )
        cases = [case for case in qs.order_by('created') if not case.has_bad_photo]

        return cases

    def get_context_data(self, *args, **kwargs):
        context = super(FreshAppsView, self).get_context_data(*args, **kwargs)
        context["workshops"] = workshops_dict()
        return context


class PhotoChangeRequestedAppsView(NaborRequiredMixin, generic.ListView):
    template_name = "intake/staff/photo_change_requested_apps.html"
    context_object_name = "applications"
    paginate_by = 50

    def get_queryset(self):
        users = User.objects.filter(userprofile__is_nabor=False)
        apps = get_fresh_apps_for_nabor(users)
        qs = ApplicationCase.objects.filter(
            user__in=apps, is_closed=False, is_suspicious=False
        )
        cases = [case for case in qs.order_by('created') if not case.has_bad_photo]
        return cases

    def get_context_data(self, *args, **kwargs):
        context = super(PhotoChangeRequestedAppsView, self).get_context_data(
            *args, **kwargs
        )
        context["workshops"] = workshops_dict()
        return context


class BlackListView(NaborRequiredMixin, generic.ListView):
    template_name = "intake/staff/blacklist.html"
    context_object_name = "twats"
    paginate_by = 150

    def get_queryset(self):
        return BlackListEntry.objects.all()

    def get_context_data(self, *args, **kwargs):
        context = super(BlackListView, self).get_context_data(*args, **kwargs)
        context["twat_categories"] = BlackListCategory.objects.all()
        return context


class WorkshopFreshAppsView(NaborRequiredMixin, generic.ListView):
    template_name = "intake/staff/workshop_fresh_apps.html"
    context_object_name = "applications"
    paginate_by = 50

    def get_queryset(self):
        w_slug = self.kwargs["w_slug"]
        qs = WorkshopPetitionConnection.objects.filter(workshop_slug=w_slug)
        users = [p.user for p in qs]
        users = [u for u in users if not u.userprofile.is_nabor]
        qs = ApplicationCase.objects.filter(
            is_closed=False,
            user__in=filter(
                lambda u: any(
                    [
                        has_buck(u, "complete_first_step", is_latest=True),
                        has_buck_after_buck(
                            u, "been_suspicious", "complete_first_step"
                        ),
                    ]
                ),
                users,
            ),
        ).order_by("created")

        return qs

    def get_context_data(self, *args, **kwargs):
        w_slug = self.kwargs["w_slug"]
        context = super(WorkshopFreshAppsView, self).get_context_data(*args, **kwargs)

        workshops = workshops_dict()
        context["workshop_name"] = workshops[w_slug]
        context["workshops"] = workshops
        context["programs"] = [
            (p_slug, p_name)
            for _, _, p_slug, p_name in get_workshop_programs(w_slug, lean=True)
        ]
        return context


class SuspiciousAppsView(NaborRequiredMixin, generic.ListView):
    template_name = "intake/staff/suspicious_apps.html"
    context_object_name = "applications"
    paginate_by = 50

    def get_queryset(self):
        qs = ApplicationCase.objects.filter(is_closed=False, is_suspicious=True)
        qs = qs.order_by("created")

        return qs


class RejectedAppsView(NaborRequiredMixin, generic.ListView):
    template_name = "intake/staff/rejected_apps.html"
    context_object_name = "applications"
    paginate_by = 50

    def get_queryset(self):
        # TODO: this should be done by manager
        qs = ApplicationCase.objects.filter(is_closed=True)
        users = [a.user for a in qs]
        rejected = filter_users_by_buck_slug(users, "nabor_rejected")

        qs = ApplicationCase.objects.filter(user__in=rejected)
        qs = qs.order_by("created")

        return qs

    def get_context_data(self, *args, **kwargs):
        context = super(RejectedAppsView, self).get_context_data(*args, **kwargs)
        context["in_rejected"] = True
        return context


class PassedAppsView(NaborRequiredMixin, generic.ListView):
    template_name = "intake/staff/passed_apps.html"
    context_object_name = "applications"
    paginate_by = 50

    def my_queryset_by_buck(self, buck_slug):
        # TODO: this should be done by manager
        qs = ApplicationCase.objects.filter(is_closed=True)
        users = [a.user for a in qs]
        accepted = filter_users_by_buck_slug(users, buck_slug)

        qs = ApplicationCase.objects.filter(user__in=accepted)
        qs = qs.order_by("-last_modified")

        return qs

    def get_queryset(self):
        return self.my_queryset_by_buck("passed_first_step")

    def get_context_data(self, **kwargs):
        context = super(PassedAppsView, self).get_context_data(**kwargs)

        context["workshops"] = [
            {"slug": k, "name": v} for k, v in workshops_dict().items()
        ]
        return context


class AllAcceptedApps(PassedAppsView):
    template_name = "intake/staff/accepted_apps.html"

    def get_queryset(self):
        return self.my_queryset_by_buck("accepted_by_workshop")


class WorkshopAcceptedApps(PassedAppsView):
    template_name = "intake/staff/workshop_accepted_apps.html"
    context_object_name = "applications"
    paginate_by = 50

    def get_queryset(self):
        w_slug = self.kwargs["w_slug"]
        return get_workshop_accepted_apps(w_slug)

    def get_context_data(self, **kwargs):
        context = super(WorkshopAcceptedApps, self).get_context_data(**kwargs)

        w_slug = self.kwargs["w_slug"]
        context["workshop_slug"] = w_slug
        context["workshop_name"] = workshops_dict().get(w_slug)
        context["programs"] = [
            {"slug": wp.program_slug, "name": wp.program_name}
            for wp in get_workshop_programs(w_slug)
        ]
        return context


class WorkshopProgramAcceptedApps(WorkshopAcceptedApps):
    template_name = "intake/staff/workshop_program_accepted_apps.html"
    context_object_name = "applications"
    paginate_by = 50

    def get_queryset(self):
        # TODO: this should be done by manager
        w_slug = self.kwargs["w_slug"]
        p_slug = self.kwargs["p_slug"]

        return get_wp_accepted_apps(w_slug, p_slug)

    def get_context_data(self, **kwargs):
        context = super(WorkshopProgramAcceptedApps, self).get_context_data(**kwargs)

        p_slug = self.kwargs["p_slug"]
        context["program_slug"] = p_slug
        return context


class WorkshopDeclinedApps(NaborRequiredMixin, generic.ListView):
    template_name = "intake/staff/workshop_declined.html"
    context_object_name = "applications"
    paginate_by = 50

    def get_queryset(self):
        # TODO: this should be done by manager
        users = User.objects.all()
        declined = filter_users_by_buck_slug(users, "declined_by_workshop")
        retriers = filter_users_by_buck_slug(users, "retry_granted")
        users = set(declined) - set(retriers)

        qs = ApplicationCase.objects.filter(user__in=users)
        qs = qs.order_by("-last_modified")

        return qs

    def get_context_data(self, *args, **kwargs):
        context = super(WorkshopDeclinedApps, self).get_context_data(*args, **kwargs)
        context["in_wdeclined"] = True
        context["workshops"] = workshops_dict()

        return context


class WorkshopDeclinedAppsSpecific(NaborRequiredMixin, generic.ListView):
    template_name = "intake/staff/workshop_declined_specific.html"
    context_object_name = "applications"
    paginate_by = 50

    def get_queryset(self):
        w_slug = self.kwargs["w_slug"]
        qs = get_workshop_declined_apps(w_slug)
        print(qs)
        users = User.objects.all()
        declined = filter_users_by_buck_slug(users, "declined_by_workshop")
        retriers = filter_users_by_buck_slug(users, "retry_granted")
        users = set(declined) - set(retriers)

        qs = ApplicationCase.objects.filter(user__in=users)
        qs = qs.order_by("-last_modified")
        return qs

    def get_context_data(self, *args, **kwargs):
        context = super(WorkshopDeclinedAppsSpecific, self).get_context_data(
            *args, **kwargs
        )
        context["in_wdeclined"] = True
        w_dict = workshops_dict()
        context["workshops"] = w_dict
        w_slug = self.kwargs.get("w_slug")
        context["current_w_slug"] = w_slug
        context["current_w_name"] = w_dict[w_slug]

        return context


class AppDetailView(NaborRequiredMixin, generic.DetailView):
    model = ApplicationCase
    template_name = "intake/staff/application_detail.html"

    def get_context_data(self, **kwargs):
        context = super(AppDetailView, self).get_context_data(**kwargs)
        context["form"] = ApplicationCommentForm(
            initial={"application": context["object"]}
        )
        return context


class AppCommentView(NaborRequiredMixin, generic.FormView):
    form_class = ApplicationCommentForm
    template_name = "intake/staff/application_detail.html"  # This is never used
    context_object_name = "comment"

    def form_valid(self, form):
        app = form.save(commit=False)
        app.author = self.request.user
        app.save()
        return redirect("intake:app_detail", pk=app.application.pk)

    def form_invalid(self, form):
        messages.error(self.request, u"Ошибка #ACV-FI")
        return redirect("intake:fresh_apps")

    def get_context_data(self, **kwargs):
        context = super(AppCommentView, self).get_context_data(**kwargs)
        context["object"] = context["object"].application
        return context


class AppSearch(NaborRequiredMixin, generic.ListView):
    template_name = "intake/staff/app_search.html"
    context_object_name = "applications"

    def get_queryset(self):
        if self.request.GET.get("search_1", False):
            s1 = self.request.GET.get("search_1")
            return ApplicationCase.objects.filter(pk=s1)

        substr = self.request.GET.get("search_0", "").split()
        if len(substr) == 3:
            q = Q(user__email__icontains=substr[-1][1:-1])
            q &= Q(user__last_name__icontains=substr[1])
            q &= Q(user__first_name__icontains=substr[0])
            qs = ApplicationCase.objects.filter(q).order_by("created")[:10]
        else:
            substr = self.request.GET.get("search_0", "")
            if substr != "":
                if substr.find(u"@") != -1:
                    q = Q(user__email__icontains=substr)
                else:
                    q = Q(user__last_name__icontains=substr)
                    q |= Q(user__first_name__icontains=substr)
                qs = ApplicationCase.objects.filter(q).order_by("created")[:10]
            else:
                qs = []
        return qs


@intake_restricted()
def accept_app(request, app_pk):
    """ Одобрить заявку
        (TODO: и направить её в мастерскую)
    """

    app = get_object_or_404(ApplicationCase, pk=app_pk)

    if attach_buck(app.user, "passed_first_step") or has_buck_after_buck(
        app.user, "retry_granted", "declined_by_workshop"
    ):
        app.accept()
        app.mark_passed()
        app.save()
        comment_on_case(
            case=app, author=get_user(request), comment=u"[auto] (одобряю эту заявку)"
        )
        messages.success(request, u"%s (#%s) отмечена как одобренная." % (app, app.pk))

        if prepare_second_round(app):
            subject = u"ЛШ2020: вступительное задание"

            tmpl = WorkshopProgramEmailTemplate.get_after_nabor_for_app(app)
            if tmpl is not None:
                text = tmpl.render()
                robot_comment = u"На выбранной программе участнику уходит письмо с вступительным. Оно ушло участнику."
                attach_buck(app.user, "email_template_passed_nabor_auto_sentt")
            else:
                template = loader.get_template("intake/staff/emails/second_round.txt")
                text = template.render({})
                robot_comment = u"У выбранной программы есть авторассылка вступительных заданий. Участнику отправлено уведомление."

            send_mail_to_applicant(app, subject, text)
            robot_comment_on_case(case=app, comment=robot_comment)

    else:
        messages.error(request, u"Заявка уже была одобрена")

    if has_buck_after_buck(app.user, "retry_granted", "declined_by_workshop"):
        # FIXME: change redirect to this wp declined apps
        return redirect("intake:workshop_declined_apps")

    return redirect("intake:fresh_apps")


@intake_restricted()
def mark_suspicious(request, app_pk):
    """ Пометить заявку подозрительной """

    app = get_object_or_404(ApplicationCase, pk=app_pk)
    u = get_user(request)

    if attach_buck(
        app.user,
        "been_suspicious",
        intention=u"Пометил {0.first_name} {0.last_name}".format(u),
    ):
        app.is_suspicious = True
        app.save()
        messages.success(
            request, u"%s (#%s) отмечена как подозрительная." % (app, app.pk)
        )
    else:
        messages.error(request, u"Заявка уже была отмечена как подозрительная")

    return redirect("intake:fresh_apps")


@intake_restricted()
def demark_suspicious(request, app_pk):
    """ Пометить заявку нормальной и вернуть в свежак """

    app = get_object_or_404(ApplicationCase, pk=app_pk)

    if has_buck(app.user, "been_suspicious"):
        app.is_suspicious = False
        app.save()
        messages.success(request, u"%s (#%s) перемещена в 'свежак'." % (app, app.pk))
    else:
        messages.error(request, u"Заявка не была отмечена как подозрительная")

    return redirect("intake:fresh_apps")


class RejectAppFromNabor(NaborRequiredMixin, generic.View):
    template_name = "intake/staff/reject_app_from_nabor.html"
    form_class = AppEmailForm

    def get_back_urls(self, app_pk):
        url_apps = reverse("intake:fresh_apps")
        url_app_detail = reverse("intake:app_detail", kwargs={"pk": app_pk})
        return [(url_app_detail, u"Назад к анкете"), (url_apps, u"Назад в свежак")]

    def get(self, request, *args, **kwargs):
        app = get_object_or_404(ApplicationCase, pk=self.kwargs["pk"])

        form = self.form_class()
        form.fields["subject"].initial = u"[ЛШ2020] Отказ в участии"
        t = loader.get_template("intake/staff/emails/reject_application.txt")
        form.fields["text"].initial = t.render(
            {
                "user_profile": get_user(request).userprofile,
                "app_first_name": app.user.first_name,
            }
        )

        return render(
            request,
            self.template_name,
            {"form": form, "back_urls": self.get_back_urls(app.pk)},
        )

    def post(self, request, *args, **kwargs):
        app = get_object_or_404(ApplicationCase, pk=self.kwargs["pk"])

        form = self.form_class(request.POST)
        if form.is_valid():
            b = attach_buck(app.user, "nabor_rejected", get_buck=True)
            app.is_closed = True
            app.save()
            messages.success(request, u"%s (#%s) отмечена как 'отказ'." % (app, app.pk))

            mejl = form.save(commit=False)
            mejl.author = get_user(request)
            mejl.to_user = app.user
            mejl.relevant_buck = b
            mejl.save()

            if send_mail_to_applicant(app, mejl.subject, mejl.text):
                messages.success(request, u"Пользователь уведомлён.")
            else:
                messages.error(
                    request,
                    u"Не получилось отправить пользователю письмо, проблемы с сервером.",
                )

            messages.info(request, u"В таймлайне появится дубликат письма.")

            comment_on_case(
                case=app,
                author=mejl.author,
                comment=u"[auto] Отказали из набора. На почту отправлен отказ.",
            )

            return redirect("intake:app_detail", app.pk)

        return render(
            request,
            self.template_name,
            {"form": form, "back_urls": self.get_back_urls(app.pk)},
        )


@intake_restricted()
def unreject_app(request, app_pk):
    """ Разрулить ситуацию "случайно нажал не ту кнопку" """

    app = get_object_or_404(ApplicationCase, pk=app_pk)

    if detach_buck(app.user, "nabor_rejected"):
        app.is_closed = False
        app.save()
        comment_on_case(
            case=app,
            author=get_user(request),
            comment=u"[auto] Возвращаю заявку из отказников.",
        )

        last_b = get_buck_nice_name(get_latest_buck(app.user).slug)

        messages.success(
            request,
            u"%s (#%s) из отказанных убрана. См. раздел '%s'. Если там нет, то смотри свежак"
            % (app, app.pk, last_b),
        )
    else:
        messages.error(request, u"Заявка не была отказником")

    return redirect("intake:fresh_apps")


@intake_restricted()
def reopen_app_case(request, app_pk):
    """ Открыть дело по заявке, снова. """

    app = get_object_or_404(ApplicationCase, pk=app_pk)

    if app.is_closed:
        app.is_closed = False
        app.save()
        comment_on_case(
            case=app,
            author=get_user(request),
            comment=u"[auto] Переоткрываю дело по заявке.",
        )
        messages.success(request, u"%s (#%s) снова открыта" % (app, app.pk))
    else:
        messages.error(request, u"Дело по заявке уже было открыто")

    return redirect("intake:workshop_declined_apps")


@intake_restricted()
def return_app(request):
    """POST method that returns app to it's initial state (pre-apply). Detachs all the bucks that were set for this user
    Expects the following fields in post:

        'app_pk': pk of the application in question
        '
    """
    if request.method != "POST":
        messages.error(request, u"Так не работает. Напишите кому-то об этом.")
        return redirect("profile")

    user = get_user(request)
    back_url = request.POST.get("back_url", "profile")

    a_pk = request.POST.get("a_pk", None)
    message_text = request.POST.get("message_text", None)

    if any(x is None for x in [a_pk, message_text]):
        messages.error(request, u"Заявку не удалось переоткрыть.")
        return redirect(back_url)

    app = get_object_or_404(ApplicationCase, pk=a_pk)
    comment_on_case(
        case=app,
        author=get_user(request),
        comment=u"[auto] Возвращаем заявку участнику. Убрали все пометки",
    )
    app.is_closed = False
    app.save()
    messages.success(request, u"%s (#%s) возвращена участнику" % (app, a_pk))

    UserBuck.objects.filter(user=app.user).delete()

    mejl = AppEmail(
        author=user,
        to_user=app.user,
        subject=u"[ЛШ2020] Ваша заявка возвращена на перезаполнение",
        text=message_text,
    )
    mejl.save()

    send_mail_to_applicant(app, mejl.subject, mejl.text)

    return redirect(back_url)


class RequestPhotoChange(NaborRequiredMixin, generic.View):
    template_name = "intake/staff/request_photo_change.html"
    form_class = AppEmailForm

    def get_back_urls(self, app_pk):
        url_apps = reverse("intake:fresh_apps")
        url_app_detail = reverse("intake:app_detail", kwargs={"pk": app_pk})
        return [(url_app_detail, u"Назад к анкете"), (url_apps, u"Назад в свежак")]

    def get(self, request, *args, **kwargs):
        app = get_object_or_404(ApplicationCase, pk=self.kwargs["pk"])

        form = self.form_class()
        form.fields["subject"].initial = u"[ЛШ2020] Просьба сменить фотографию"
        t = loader.get_template("intake/staff/emails/request_photo_change.txt")
        form.fields["text"].initial = t.render(
            {
                "user_profile": get_user(request).userprofile,
                "app_first_name": app.user.first_name,
            }
        )

        return render(
            request,
            self.template_name,
            {"form": form, "back_urls": self.get_back_urls(app.pk)},
        )

    def post(self, request, *args, **kwargs):
        app = get_object_or_404(ApplicationCase, pk=self.kwargs["pk"])

        photo = get_object_or_404(UserPhoto, user=app.user)

        form = self.form_class(request.POST)
        if form.is_valid():
            mejl = form.save(commit=False)
            mejl.author = get_user(request)
            mejl.to_user = app.user
            mejl.save()

            if send_mail_to_applicant(app, mejl.subject, mejl.text):
                messages.success(request, u"Пользователь уведомлён.")
            else:
                messages.error(
                    request,
                    u"Не получилось отправить пользователю письмо, проблемы с сервером.",
                )

            messages.info(
                request,
                u"В таймлайне пользователя появится информация, дублирующая отправленное письмо.",
            )
            photo.needs_to_be_changed = True
            photo.save()

            comment_on_case(
                case=app,
                author=mejl.author,
                comment=u"[auto] Отправлен запрос на смену фото.",
            )

            return redirect("intake:app_detail", app.pk)

        return render(
            request,
            self.template_name,
            {"form": form, "back_urls": self.get_back_urls(app.pk)},
        )


class ChangeAppWorkshop(NaborRequiredMixin, generic.View):
    template_name = "intake/staff/change_app_workshop.html"
    form_class = WPChangeForm

    def get_back_urls(self, app_pk):
        url_apps = reverse("intake:fresh_apps")
        url_app_detail = reverse("intake:app_detail", kwargs={"pk": app_pk})
        return [(url_app_detail, u"Назад к анкете"), (url_apps, u"Назад в свежак")]

    def get(self, request, *args, **kwargs):
        app = get_object_or_404(ApplicationCase, pk=self.kwargs["pk"])

        form = self.form_class(instance=app.user.workshoppetitionconnection)

        return render(
            request,
            self.template_name,
            {"application": app, "form": form, "back_urls": self.get_back_urls(app.pk)},
        )

    def post(self, request, *args, **kwargs):
        app = get_object_or_404(ApplicationCase, pk=self.kwargs["pk"])

        wpc = get_object_or_404(WorkshopPetitionConnection, user=app.user)
        wpc_old = u"%s/%s" % (wpc.workshop_slug, wpc.program_slug)

        form = self.form_class(request.POST, instance=wpc)
        if form.is_valid():
            wpc_new = u"{0.workshop_slug}/{0.program_slug}".format(form.save())
            from_to_str = u"с {} на {}.".format(wpc_old, wpc_new)
            messages.success(request, u"Мастерская изменилась %s." % from_to_str)
            comment_on_case(
                case=app,
                author=get_user(request),
                comment=u"[auto] Мастерская изменена %s" % from_to_str,
            )

            case_user = app.user
            if not attach_buck(case_user, "workshop_was_changed"):
                lick_buck("workshop_was_changed")

            if has_buck(case_user, "declined_by_workshop"):
                if not attach_buck(case_user, "retry_granted"):
                    lick_buck("retry_granted")
                return redirect("intake:accept_app", app.pk)

            return redirect("intake:app_detail", app.pk)

        return render(
            request,
            self.template_name,
            {"application": app, "form": form, "back_urls": self.get_back_urls(app.pk)},
        )
