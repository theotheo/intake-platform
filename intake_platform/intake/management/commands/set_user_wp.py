# coding: utf-8

from django.core.management.base import BaseCommand

from django.contrib.auth.models import User

from intake_platform.intake.models import WorkshopProgramMetaInfo
from intake_platform.intake.tools import set_curator


class Command(BaseCommand):
    help = "Sets the user as a curator for workshop program"

    def add_arguments(self, parser):
        parser.add_argument("email", help="user email substring", type=str)
        parser.add_argument("w_slug", help="workshop slug substring", type=str)
        parser.add_argument("p_slug", help="program slug substring", type=str)

    def handle(self, *args, **options):
        u = User.objects.get(email__icontains=options["email"])

        wp = WorkshopProgramMetaInfo.objects.get(
            program_slug=options["p_slug"], workshop_slug=options["w_slug"]
        )
        print("--- Fond WP ----")
        print(wp)

        print("--- Found user ---")
        print(u.username)
        print("%s %s" % (u.first_name, u.last_name))
        print(u.email)

        set_curator(u, options["w_slug"], [options["p_slug"]])
        print("WP Curators now:")
        for c in wp.curators.all():
            print("  {} ({})".format(c, c.email))
