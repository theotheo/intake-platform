# coding: utf-8

from django.core.management.base import BaseCommand
from django.contrib.auth.models import User

from intake.tools import has_buck


class Command(BaseCommand):
    help = "Dumps all emails of apps passed nabor"

    def handle(self, *args, **options):
        bucks = ["passed_idiot_test", "complete_first_step", "passed_first_step"]

        for b in bucks:
            for u in [
                x
                for x in list(User.objects.all())
                if all([has_buck(x, b) for b in bucks])
            ]:
                print(u.email)
