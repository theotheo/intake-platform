# coding: utf-8

from django.core.management.base import BaseCommand

from collections import Counter

from intake.tools import workshops_dict, get_workshop_accepted_apps


class Command(BaseCommand):
    help = "Pings orgs chat some counters"

    def handle(self, *args, **options):
        apps = []
        ws_dict = workshops_dict()
        for w_slug, _ in ws_dict.items():
            apps += get_workshop_accepted_apps(w_slug)
        males = [x for x in apps if x.user.userprofile.is_male]
        male_names = [f.user.first_name.lower() for f in males]
        for name, occurancy in Counter(male_names).most_common(10):
            print(name, occurancy)

        print("---------")

        females = [x for x in apps if not x.user.userprofile.is_male]
        female_names = [f.user.first_name.lower() for f in females]
        for name, occurancy in Counter(female_names).most_common(10):
            print(name, occurancy)

        print("На ЛШ принято %s Ульян" % Counter(female_names)["ульяна"])
        print("---------")

        for m in males:
            if m.user.first_name.lower().startswith("илья"):
                print(
                    m.user.last_name,
                    m.user.userprofile.age,
                    m.user.workshoppetitionconnection.workshop_slug,
                )

        print("---------")
        for m in females:
            if m.user.first_name.lower().startswith(("анна", "анастасия")):
                w = m.user.workshoppetitionconnection.workshop_slug
                if w in ["andan", "datascience"]:
                    print(
                        m.user.first_name, m.user.last_name, m.user.userprofile.age, w
                    )
            if m.user.first_name.lower().startswith("ульяна"):
                w = m.user.workshoppetitionconnection.workshop_slug
                print(m.user.first_name, m.user.last_name, m.user.userprofile.age, w)
