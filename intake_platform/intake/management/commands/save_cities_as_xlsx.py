# coding: utf-8

from django.core.management.base import BaseCommand
import openpyxl
from openpyxl.cell import get_column_letter
from datetime import datetime
from collections import Counter

from intake_platform.common.models import UserProfile

# import pprint
from intake_platform.intake.models import User, UserEducationInfo
from intake_platform.intake.tools import has_buck


class Command(BaseCommand):
    help = "Show various info and stats"
    LOCATION_COUNT = 10

    def get_distinct_locations_info(self, users):
        """ Get location stats info on the given users
        """

        locations = [
            u.location.lower().strip()
            for u in UserProfile.objects.filter(user__in=users)
        ]
        c = Counter(locations)
        return c

    def handle(self, *args, **options):
        # Find users with buck passed

        buck_slugs_to_check = ["accepted_by_workshop"]

        users = {b: [] for b in buck_slugs_to_check}
        for b in buck_slugs_to_check:
            users[b] = [
                x
                for x in User.objects.filter(userbuck__slug=b).all()
                if (not has_buck(x, "son_of_a_bitch"))
            ]

        u_list = users.pop(buck_slugs_to_check[0])
        loc_count = self.get_distinct_locations_info(u_list)
        print("Всего различных локаций: %s" % len(list(loc_count.keys())))
        print("%s самых популярные локации:" % self.LOCATION_COUNT)
        for l, v in loc_count.most_common(self.LOCATION_COUNT):
            print("  %s (%s)" % (l, v))

        print("Дубна: ", loc_count["дубна"])

        ###################
        now_str = datetime.now().strftime("%Y%b%d").lower()
        fname = "locations_%s.xlsx" % now_str

        workbook = openpyxl.Workbook()  # outfile, {'in_memory': True})
        worksheet = workbook.get_active_sheet()

        for tag, col_num, width in [("Город", 1, 80), ("Счётчик", 2, 10)]:
            c = worksheet.cell(row=1, column=col_num)
            c.value = tag
            c.style.font.bold = True
            column_letter = get_column_letter(col_num)
            worksheet.column_dimensions[column_letter].width = width

        for i, (city, response_count) in enumerate(loc_count.most_common(), start=2):
            lhs = worksheet.cell(row=i, column=1)
            lhs.value = city
            lhs.style.alignment.wrap_text = True

            rhs = worksheet.cell(row=i, column=2)
            rhs.value = response_count
            rhs.style.alignment.wrap_text = True

        workbook.save(fname)
        return "Check out '%s'" % fname
