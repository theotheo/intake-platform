# coding: utf-8

from time import sleep

from django.core.management.base import BaseCommand
from intake_platform.intake.models import ApplicationCase
from intake_platform.intake.tools import (
    filter_users_by_buck_slug,
    has_buck,
    prepare_second_round,
    robot_comment_on_case,
    send_mail_to_applicant,
)


class Command(BaseCommand):
    help = "Send Workshop Programs assignments"

    def handle(self, *args, **options):
        # find all users who completed_first_step
        qs = ApplicationCase.objects.filter(is_closed=True)
        users = [a.user for a in qs]

        subject = "ЛШ2020: вступительное задание"
        text = """Добрый день.

Вам предлагается выполнить вступительное задание для выбранной программы.

Чтобы это сделать, необходимо войти на сайт https://letnyayashkola.ru и проследовать указанным там инструкциям.

Всего доброго
Сара Коннор
"""

        counter = 0
        for u in filter_users_by_buck_slug(users, "passed_first_step"):
            if has_buck(u, "assignment_auto_sent"):
                print("User #%s has this buck" % u.pk)
                continue

            print(u.pk)
            case = ApplicationCase.objects.get(user=u)

            if prepare_second_round(case, intention="Management command"):
                counter += 1
                robot_comment_on_case(
                    case=case,
                    comment="У выбранной программы есть автоматическое задание 2-го тура. Скрипт выслал участнику уведомление.",
                )
                send_mail_to_applicant(case, subject, text)
                sleep(1)

        print("total cases fixed:", counter)
