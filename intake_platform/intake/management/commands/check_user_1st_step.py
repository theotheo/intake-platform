# coding: utf-8

from django.core.management.base import BaseCommand

from django.contrib.auth.models import User
from annoying.functions import get_object_or_None

from intake.tools import first_step_completed
from intake.models import (
    WorkshopPetitionConnection,
    UserAdditionalContacts,
    UserEducationInfo,
    UserActivityInfo,
    UserHandymanshipInfo,
    GiveBackInfo,
    UserLogisticsInfo,
    PastFuturePresentInfo,
    UserPhoto,
    UnderageUserInfo,
)


class Command(BaseCommand):
    help = "Checks what data is filled"

    def add_arguments(self, parser):
        parser.add_argument("email", help="substring", type=str)

    def handle(self, *args, **options):

        users = User.objects.filter(email__icontains=options["email"])
        for u in users:
            related_cls = [
                ("WorkshopPetitionConnection", WorkshopPetitionConnection),
                ("UserAdditionalContacts", UserAdditionalContacts),
                ("UserEducationInfo", UserEducationInfo),
                ("UserActivityInfo", UserActivityInfo),
                ("UserHandymanshipInfo", UserHandymanshipInfo),
                ("GiveBackInfo", GiveBackInfo),
                ("UserLogisticsInfo", UserLogisticsInfo),
                ("PastFuturePresentInfo", PastFuturePresentInfo),
                ("UserPhoto", UserPhoto),
            ]
            if u.userprofile.is_underage:
                related_cls.append(("UnderageUserInfo", UnderageUserInfo)),

            print("------")
            print("User: %s" % u)
            print("email: %s" % u.email)
            for name, c in related_cls:
                obj = get_object_or_None(c, user=u)
                print(
                    "[{}]: {}".format(
                        name, ("pk= %s" % obj.pk) if obj is not None else "--"
                    )
                )
            print(
                "------\nFirst step completed? %s" % first_step_completed(u.userprofile)
            )
