# coding: utf-8

from django.core.management.base import BaseCommand
from intake.models import UserBuck
from intake.tools import programs_dict, workshops_dict


class Command(BaseCommand):
    help = "Shows accepted/applied applicants data per years"

    def handle(self, *args, **options):
        slugs_of_interest = (
            "passed_first_step",
            "son_of_a_bitch",
            "accepted_by_workshop",
            "nabor_rejected",
            "declined_by_workshop",
            "been_suspicious",
        )
        counts = {}
        for db_tag, data_year in [
            ("default", 2019),
            ("data_2018", 2018),
            ("data_2017", 2017),
            ("data_2016", 2016),
            ("data_2015", 2015),
        ]:
            counts[data_year] = {
                slug: UserBuck.objects.using(db_tag).filter(slug=slug).count()
                for slug in slugs_of_interest
            }
            counts[data_year]["total_ws"] = len(list(workshops_dict(db_tag).keys()))
            counts[data_year]["total_wp"] = len(list(programs_dict(db_tag).keys()))
        print(
            "\n--------\n".join(
                [
                    """В {year} году было у нас было {total_ws} мастерских (и {total_wp} всего программ).
Судя по сайту, приехало {total_arrived} участников, а всего подано {total_passed_nabor} (прошло через группу набор). Было ещё {total_cunts} принятых, но не доехавших по разным причинам.
Из принятых НЕ доезжает {total_faktisk_arrived_perc:.2f}%.
Набор отказал {total_nabor_reject} участникам, а сами мастерские отказались от {total_ws_declined} заявок. Подозрительных заявок (таких что они по какой-то причине вызвали подозрение группы набор) было {total_been_suspicious}.
""".format(
                        year=year,
                        total_ws=counts[year]["total_ws"],
                        total_wp=counts[year]["total_wp"],
                        total_arrived=counts[year]["accepted_by_workshop"]
                        - counts[year]["son_of_a_bitch"],
                        total_passed_nabor=counts[year]["passed_first_step"],
                        total_cunts=counts[year]["son_of_a_bitch"],
                        total_faktisk_arrived_perc=100
                        * counts[year]["son_of_a_bitch"]
                        / float(counts[year]["accepted_by_workshop"]),
                        total_nabor_reject=counts[year]["nabor_rejected"],
                        total_ws_declined=counts[year]["declined_by_workshop"],
                        total_been_suspicious=counts[year]["been_suspicious"],
                    )
                    for year in (2015, 2016, 2017, 2018, 2019)
                ]
            )
        )
