# coding: utf-8

from collections import defaultdict

from django.core.management.base import BaseCommand
from datetime import datetime as dt

from common.utils import UnicodeWriter
from common.models import UserProfile

from intake.models import WorkshopPetitionConnection, UserBuck
from intake.tools import workshops_dict, get_workshop_programs, has_buck

USING_DB = "data_2017"
RELATIVE_AGE_DATE_TUPLE = (2017, 7, 7)


def give_up_age(user_profile):
    if user_profile.birthdate is not None:
        return str(user_profile.age_on_date(RELATIVE_AGE_DATE_TUPLE))
    else:
        return "N/A"


class Command(BaseCommand):
    help = "Dump full names as csv in the current dir as 'full_names.csv'"

    def handle(self, *args, **options):
        with open("./full_names_%s.csv" % USING_DB, "w") as fw:
            csvwriter = UnicodeWriter(fw)
            for up in UserProfile.objects.using(USING_DB).all():
                csvwriter.writerow(
                    [str(up.user.pk), up.user.first_name, up.middle_name, up.last_name]
                )
            print(("full_names_%s.csv" % USING_DB))

            with open("./ages_location_%s.csv" % USING_DB, "w") as fw:
                csvwriter = UnicodeWriter(fw)
                for up in UserProfile.objects.using(USING_DB).all():
                    csvwriter.writerow(
                        [str(up.user.pk), give_up_age(up), up.location or "N/A"]
                    )
            print(("ages_location_%s.csv" % USING_DB))


# vim: tabstop=2 softtabstop=2 shiftwidth=2 textwidth=159
