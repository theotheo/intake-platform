from django.core.management.base import BaseCommand

from intake_platform.intake.common.models import UserRestrictions


class Command(BaseCommand):
    help = 'Print out available user restrictions'

    def handle(self, *args, **options):
        for r in UserRestrictions.objects.all():
            print(r)
        else:
            print('No restrictions were found')
