# coding: utf-8

from django.core.management.base import BaseCommand

from intake.tools import get_wp_apps_counts_dict, workshops_dict, has_buck
from intake.models import WorkshopPetitionConnection


class Command(BaseCommand):
    help = "Lists workshops by their transition coefficients"

    def handle(self, *args, **options):
        hardest_pass = {"slug": "", "transition_coefficient": 1}
        ws_dict = workshops_dict()
        for w_slug, w_name in ws_dict.items():
            bucks_dict = get_wp_apps_counts_dict(w_slug)
            wp_passed_apps_total = sum(
                [
                    has_buck(wpc.user, "passed_first_step")
                    for wpc in WorkshopPetitionConnection.objects.filter(
                        workshop_slug=w_slug
                    )
                ]
            )
            if wp_passed_apps_total == 0:
                continue
            transition_coeff = (
                1.0 - float(bucks_dict["declined"]) / wp_passed_apps_total
            )
            print(
                """==> {0} {1}
  TOTAL {2}, accepted {3}. Transition coeff: {4}""".format(
                    w_slug,
                    w_name,
                    bucks_dict["total"],
                    bucks_dict["accepted"],
                    transition_coeff,
                )
            )
            if transition_coeff < hardest_pass["transition_coefficient"]:
                hardest_pass["slug"] = w_slug
                hardest_pass["transition_coefficient"] = transition_coeff

        print("==> Hardest: %s <==" % hardest_pass["slug"])
        print("  %s" % ws_dict[hardest_pass["slug"]])
        print("  Coeff: %s" % hardest_pass["transition_coefficient"])
