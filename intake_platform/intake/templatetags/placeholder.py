from django.template import Library

register = Library()


@register.filter
def placeholder(value, token):
    value.field.widget.attrs["placeholder"] = token
    return value


@register.filter
def addclass(value, token):
    try:
        value.field.widget.attrs["class"] += " %s" % token
    except KeyError:
        value.field.widget.attrs["class"] = token
    return value
