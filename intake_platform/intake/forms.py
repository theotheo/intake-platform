# coding: utf-8

from crispy_forms.bootstrap import FormActions
from crispy_forms.helper import FormHelper
from crispy_forms.layout import HTML, Div, Field, Layout, Submit  # , Hidden
from django import forms
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from selectable.forms import AutoCompleteWidget
from intake_platform.common.lookups import LocationLookup

from .models import (
    AppEmail,
    ApplicationAssignmentResponse,
    ApplicationCaseComment,
    GiveBackInfo,
    PastFuturePresentInfo,
    UnderageUserInfo,
    UserActivityInfo,
    UserAdditionalContacts,
    UserEducationInfo,
    UserHandymanshipInfo,
    UserLogisticsInfo,
    WorkshopPetitionConnection,
)
from .widgets import CheckboxSelectMultiple

LEAKAGE_CHOICES = [
    (0, u"от знакомых"),
    (1, u'из журнала "Русский Репортёр"'),
    (2, u'из журнала "Кот Шрёдингера"'),
    (3, u"в социальных сетях"),
    (4, u"где-то ещё в Интернете"),
]

PHONE_FIELD_LENGTH = 20


class UserNotDumbForm(forms.Form):
    question1_smoking = forms.ChoiceField(
        label=u"Можно курить:",
        choices=[
            (0, u"Возле реки."),
            (100, u"В специально отведенных местах за территорией лагеря."),
            (2, u"Где угодно, если вы взяли ведро с водой."),
        ],
        widget=forms.RadioSelect,
    )
    question2_fire = forms.ChoiceField(
        label=u"Можно разводить костер:",
        choices=[
            (0, u"В любом месте, где кто-то когда-то его жег. "),
            (1, u"Там, где разрешил координатор мастерской."),
            (100, u"В специально обустроенных кострищах на берегу Волги."),
        ],
        widget=forms.RadioSelect,
    )
    question3_alcotrash = forms.ChoiceField(
        label=u"Употребление алкоголя на Летней школе:",
        choices=[
            (0, u"Разрешено при отсутствии пьяных дебошей."),
            (100, u"Запрещено."),
            (1, u"Запрещено, но если немного, то можно."),
        ],
        widget=forms.RadioSelect,
    )
    question4_dishwashing = forms.ChoiceField(
        label=u"Посуда после ужина, обеда и завтрака моется:",
        choices=[
            (100, u"Дежурными по школе на мойке рядом с кухней."),
            (1, u"Каждым участником школы в умывальниках."),
        ],
        widget=forms.RadioSelect,
    )
    question5_sick = forms.ChoiceField(
        label=u"За советом по поводу состояния здоровья, находясь на Летней школе, нужно обращаться:",
        choices=[
            (100, u"К врачу школы."),
            (1, u"К координатору мастерской."),
            (2, u"К школьнику с медицинского отделения."),
        ],
        widget=forms.RadioSelect,
    )
    question6_imdumb = forms.ChoiceField(
        label=u"При наличии какой-то проблемной ситуации (кроме проблем со здоровьем, которые к врачу), наиболее правильно будет обратиться с вопросом в первую очередь к:",
        choices=[
            (0, u"Директору Летней Школы"),
            (100, u"Куратору мастерской"),
            (2, u"Другому участнику"),
            (3, u"Преподавателю на лекции"),
        ],
        widget=forms.RadioSelect,
    )
    question7_zapevala = forms.ChoiceField(
        label=u"На школе время тишины, в течение которого громко говорить, петь и т. д. можно только в специальных местах, далеко отстоящих от палаток участников, наступает в:",
        choices=[(0, u"21.00"), (1, u"22.00"), (100, u"23.00"), (3, u"24.00")],
        widget=forms.RadioSelect,
    )

    def clean(self):
        super(forms.Form, self).clean()
        cd = self.cleaned_data

        i_haz_error = False

        for q_tail in [
            "1_smoking",
            "2_fire",
            "3_alcotrash",
            "4_dishwashing",
            "5_sick",
            "6_imdumb",
            "7_zapevala",
        ]:
            answer = "100"
            error_message = u"Нет. Попробуйте ещё раз."
            tag = "question%s" % q_tail
            if tag not in cd:
                self._errors[tag] = [u"Обязательный пункт"]
                i_haz_error = True
                continue
            if cd[tag] != answer:
                self._errors[tag] = [u"%s" % error_message]
                i_haz_error = True

        if i_haz_error:
            self.add_error(
                None, u"Надо исправить ошибки в тесте, а то как-то некрасиво получится"
            )

        return self.cleaned_data

    def __init__(self, *args, **kwargs):
        super(UserNotDumbForm, self).__init__(*args, **kwargs)

        helper = FormHelper(self)
        helper.layout = Layout(
            Div(
                Field("question1_smoking"),
                Field("question2_fire"),
                Field("question3_alcotrash"),
                Field("question4_dishwashing"),
                css_class="col-lg-6",
            ),
            Div(
                Field("question5_sick"),
                Field("question6_imdumb"),
                Field("question7_zapevala"),
                css_class="col-lg-6",
            ),
            Div(
                FormActions(Submit("submit", u"Клик", css_class="btn btn-primary")),
                css_class="form-group",
            ),
        )
        self.helper = helper


class ConfirmationForm(forms.Form):
    confirmation = forms.BooleanField(
        label=u"Ознакомлен, согласен, готов не нарушать", required=True
    )


class WPChangeForm(forms.ModelForm):
    class Meta:
        model = WorkshopPetitionConnection
        fields = ("program_slug", "workshop_slug")
        widgets = {
            "program_slug": forms.HiddenInput,
            "workshop_slug": forms.HiddenInput,
        }

    def __init__(self, *args, **kwargs):
        super(WPChangeForm, self).__init__(*args, **kwargs)

        helper = FormHelper(self)
        helper.layout = Layout(
            Field("workshop_slug", data_bind="value: workshop_slug"),
            Field("program_slug", data_bind="value: program_slug"),
            FormActions(
                Submit("submit", u"Изменить", css_class="btn btn-primary"),
                HTML(
                    u"""
                     {% for url, caption in back_urls %}
                     <a class="btn btn-default" href="{{ url }}">{{ caption }}</a>
                     {% endfor %}
                     """
                ),
            ),
        )
        self.helper = helper


class WorkshopProgramSelectForm(forms.ModelForm):
    class Meta:
        model = WorkshopPetitionConnection
        fields = ("wait_but_why", "program_slug", "workshop_slug")
        widgets = {
            "wait_but_why": forms.widgets.Textarea,
            "program_slug": forms.HiddenInput,
            "workshop_slug": forms.HiddenInput,
        }

    def __init__(self, *args, **kwargs):
        super(WorkshopProgramSelectForm, self).__init__(*args, **kwargs)

        helper = FormHelper(self)
        helper.form_tag = False
        helper.layout = Layout(
            Field('program_slug'),
            Field('workshop_slug'),
            Field(
                "wait_but_why",
                placeholder=u"Почему Вы хотите поехать на выбранную мастерскую (не менее 7 и не более 15 развернутых предложений. Здесь может быть названо желание определиться с своей возможной профессией, расширить компетенции в уже имеющейся профессии, расширить знания в области своего хобби, попробовать себя в новом виде деятельности, желание сменить обстановку и получить эмоциональную встряску, желание узнать, как устроен проект изнутри или что-то совсем другое). Если Вы работали в выбранной области – опишите свои достижения. Если в выбранной области не работали – опишите достижения в других областях – как основной деятельности, так и хобби)",
            ),
            FormActions(Submit("submit", u"Выбрал!", css_class="btn btn-primary")),
        )
        self.helper = helper


class WPCMotivationForm(forms.ModelForm):
    class Meta:
        model = WorkshopPetitionConnection
        fields = ("wait_but_why", "program_slug", "workshop_slug", "user")
        widgets = {
            "wait_but_why": forms.widgets.Textarea,
            "program_slug": forms.HiddenInput,
            "workshop_slug": forms.HiddenInput,
            "user": forms.HiddenInput,
        }

    def __init__(self, *args, **kwargs):
        super(WPCMotivationForm, self).__init__(*args, **kwargs)

        helper = FormHelper(self)
        helper.form_tag = False
        helper.layout = Layout(
            Field("user"),
            Field("workshop_slug"),
            Field("program_slug"),
            Div(Field("wait_but_why"), css_class="modal-body"),
            Div(
                FormActions(
                    HTML(
                        u'<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть без сохранения</button>'
                    ),
                    Submit("submit", u"Сохранить", css_class="btn btn-primary"),
                ),
                css_class="modal-footer",
            ),
        )
        self.helper = helper


class AdditionalContactsForm(forms.ModelForm):
    class Meta:
        model = UserAdditionalContacts
        fields = ("phone", "skype", "vk", "fb", "tg")

    def __init__(self, *args, **kwargs):
        super(AdditionalContactsForm, self).__init__(*args, **kwargs)
        helper = FormHelper(self)
        helper.layout = Layout(
            Field("phone"),
            Field("skype"),
            Field("vk"),
            Field("fb"),
            Field("tg"),
            FormActions(Submit("submit", u"Клик", css_class="btn btn-primary btn-lg")),
        )
        self.helper = helper


class EducationInfoForm(forms.ModelForm):
    class Meta:
        model = UserEducationInfo
        fields = ("finished_ed", "affiliation")
        widgets = {"affiliation": forms.Textarea, "degree": forms.Textarea}

    def __init__(self, *args, **kwargs):
        super(EducationInfoForm, self).__init__(*args, **kwargs)

        helper = FormHelper(self)

        helper.layout = Layout(
            Field("finished_ed"),
            Field(
                "affiliation",
                placeholder=u"Направление, специализация, и другие любопытные детали",
            ),
            FormActions(Submit("submit", u"Клик", css_class="btn btn-primary btn-lg")),
        )
        self.helper = helper


class ActivityInfoForm(forms.ModelForm):
    class Meta:
        model = UserActivityInfo
        fields = ("main_occupation", "hobbies")
        widgets = {"main_occupation": forms.Textarea, "hobbies": forms.Textarea}

    def __init__(self, *args, **kwargs):
        super(ActivityInfoForm, self).__init__(*args, **kwargs)

        helper = FormHelper(self)

        helper.layout = Layout(
            Field("main_occupation"),
            Field("hobbies"),
            Div(
                FormActions(
                    Submit("submit", u"Клик", css_class="btn btn-primary btn-lg")
                )
            ),
        )
        self.helper = helper


class HandymanshipInfoForm(forms.ModelForm):
    class Meta:
        model = UserHandymanshipInfo
        fields = ("relevant_skills", "can_before", "can_after")
        widgets = {"relevant_skills": forms.Textarea}

    def __init__(self, *args, **kwargs):
        super(HandymanshipInfoForm, self).__init__(*args, **kwargs)

        helper = FormHelper(self)

        helper.layout = Layout(
            Field("can_before", css_class="checkbox"),
            Field("can_after", css_class="checkbox"),
            Field(
                "relevant_skills",
                placeholder=u"Опишите те ваши навыки, которые вы, считаете, могут пригодиться",
            ),
            Div(
                FormActions(Submit("submit", u"Клик", css_class="btn btn-primary")),
                css_class="form-group",
            ),
        )
        self.helper = helper


class MoneyInfoForm(forms.ModelForm):
    class Meta:
        model = GiveBackInfo
        fields = ("money",)

    def __init__(self, *args, **kwargs):
        super(MoneyInfoForm, self).__init__(*args, **kwargs)

        helper = FormHelper(self)

        helper.form_class = "form-horizontal"
        helper.layout = Layout(
            Div(Field("money", css_class="form-control"), css_class="form-group"),
            Div(
                FormActions(Submit("submit", u"Жмяк", css_class="btn btn-primary")),
                css_class="form-group",
            ),
        )
        self.helper = helper


class GiveBackInfoForm(forms.ModelForm):
    skills = forms.MultipleChoiceField(
        label=u"Готовы ли вы помогать на школе?",
        help_text=u"Всегда нужны люди, помогающие приводить хаос в порядок",
        choices=GiveBackInfo.SKILLS_CHOICES,
        widget=CheckboxSelectMultiple,
        required=False,
    )
    skills_other = forms.CharField(
        label=u"Другое:",
        widget=forms.Textarea,
        help_text=u"Ваши уникальные способности, которые пригодятся ЛШ",
        required=False,
    )

    class Meta:
        model = GiveBackInfo
        fields = ("relevant_skills",)
        widgets = {"relevant_skills": forms.HiddenInput}

    def __init__(self, *args, **kwargs):
        super(GiveBackInfoForm, self).__init__(*args, **kwargs)

        helper = FormHelper(self)

        helper.layout = Layout(
            Field("relevant_skills"),
            Field("skills"),
            Field("skills_other"),
            FormActions(Submit("submit", u"Отправить", css_class="btn btn-primary")),
        )
        self.helper = helper


class PastFuturePresentInfoForm(forms.ModelForm):
    class Meta:
        model = PastFuturePresentInfo
        fields = ("been_before",)
        widgets = {"been_before": forms.HiddenInput}

    def __init__(self, *args, **kwargs):
        super(PastFuturePresentInfoForm, self).__init__(*args, **kwargs)

        helper = FormHelper(self)
        helper.form_tag = False

        helper.layout = Layout(
            # Field('nostalgia', placeholder=u"Здесь вы можете предаться воспоминаниям и дать какую-то обратную связь, если что-то осталось невысказанным, или что-то вас волнует перед грядущим сезоном"),
            Field("been_before", data_bind="value: ko.toJSON(schools)"),
            Div(
                FormActions(
                    Submit("submit", u"Записать", css_class="btn btn-primary btn-lg")
                )
            ),
        )
        self.helper = helper


class LogisticsInfoForm(forms.ModelForm):
    class Meta:
        model = UserLogisticsInfo
        fields = ("itsmyway",)

    def __init__(self, *args, **kwargs):
        super(LogisticsInfoForm, self).__init__(*args, **kwargs)

        helper = FormHelper(self)

        helper.form_class = "form-horizontal"
        helper.layout = Layout(
            Div(Field("itsmyway", css_class="form-control"), css_class="form-group"),
            Div(
                FormActions(
                    Submit("submit", u"Именно так", css_class="btn btn-primary")
                ),
                css_class="form-group",
            ),
        )
        self.helper = helper


class LeakageInfoForm(forms.Form):
    src_select = forms.MultipleChoiceField(
        label=u"Как вы узнали про ЛШ?",
        choices=PastFuturePresentInfo.LEAKAGE_CHOICES,
        widget=CheckboxSelectMultiple,
    )
    src_other = forms.CharField(
        label=u"Другое:", max_length=1000, widget=forms.Textarea, required=False
    )

    def __init__(self, *args, **kwargs):
        super(LeakageInfoForm, self).__init__(*args, **kwargs)

        helper = FormHelper(self)

        helper.layout = Layout(
            Field("src_select"),
            Field(
                "src_other",
                placeholder=u"Расскажите, как вы узнали про ЛШ и чем вас эта информация зацепила",
            ),
            FormActions(Submit("submit", u"Отправить", css_class="btn btn-primary")),
        )
        self.helper = helper


class PhotoUploadForm(forms.Form):
    photo = forms.ImageField(label=u"Файл на замену")

    def clean_photo(self):
        photo = self.cleaned_data.get("photo", False)

        if photo:
            if photo.size > (2 << 20):
                raise ValidationError(u"Картинка большая (> 2mb).")
        else:
            raise ValidationError(u"Не смог прочитать загруженную картинку")

        return photo


class BasicInfoForm(forms.Form):
    GENDER_CHOICES = [(1, 'Мужской'), (0, 'Женский')]

    last_name = forms.CharField(label='Фамилия', max_length=100)
    first_name = forms.CharField(label='Имя', max_length=100)
    middle_name = forms.CharField(label='Отчество', max_length=100)
    is_male = forms.TypedChoiceField(
        label='Пол',
        choices=GENDER_CHOICES,
        coerce=int,  # don't touch this field, unless you test it thoroughly
        widget=forms.RadioSelect,
    )
    location = forms.CharField(
        label='Город',
        widget=AutoCompleteWidget(LocationLookup, allow_new=True),
        max_length=400,
        required=True,
    )
    birthdate = forms.DateField(
        label='Дата рождения',
        widget=forms.widgets.SelectDateWidget(years=range(2006, 1950, -1)),
    )
    email = forms.EmailField(label="e-mail")

    @classmethod
    def from_user(cls, user):
        profile = user.userprofile
        initial = {}
        for source, field in [
            (user, 'first_name'),
            (user, 'last_name'),
            (user, 'email'),
            (profile, 'middle_name'),
            (profile, 'location'),
            (profile, 'birthdate'),
        ]:
            if getattr(source, field):
                initial[field] = getattr(source, field)
        initial['is_male'] = int(profile.is_male)

        return cls(initial=initial)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        helper = FormHelper(self)

        helper.layout = Layout(
            Field('last_name'),
            Field('first_name'),
            Field('middle_name'),
            Field('is_male'),
            Field('location'),
            Field('birthdate'),
            Field('email'),
            FormActions(Submit("submit", u"Отправить", css_class="btn btn-primary")),
        )
        self.helper = helper


class PhotoReplaceForm(forms.Form):
    photo = forms.ImageField(label="Фотка")

    def __init__(self, *args, **kwargs):
        super(PhotoReplaceForm, self).__init__(*args, **kwargs)

        helper = FormHelper(self)
        helper.form_tag = False
        helper.help_tags = False
        helper.layout = Layout(
            Div(
                HTML(
                    u"""
                     <h4 id="photo-preview-header">Старое фото</h4>
                    <img id="photo-preview" class="img-responsive" src="{{ photo.photo_absolute_url }}">
                """
                ),
                Div(Field("photo", onchange="readURL(this);")),
                css_class="modal-body",
            ),
            Div(
                FormActions(
                    HTML(
                        u'<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>'
                    ),
                    Submit("submit", u"Заменить", css_class="btn btn-primary"),
                ),
                css_class="modal-footer",
            ),
        )
        self.helper = helper


class UnderageUserInfoForm(forms.ModelForm):
    parental_confirm = forms.TypedChoiceField(
        label=u"Родители знают о моём желании поехать на ЛШ",
        coerce=lambda x: x == "YES",
        initial="NO",
        choices=(("NO", u"Нет"), ("YES", u"Да")),
        widget=forms.RadioSelect,
    )
    parental_phone = forms.CharField(
        label=u"Телефон одного из родителей",
        max_length=PHONE_FIELD_LENGTH,
        validators=[
            RegexValidator(
                r"[0-9]+$", u"Номер может состоять только из цифры и символов -,+,(,)"
            )
        ],
    )
    parental_name = forms.CharField(label=u"ФИО одного из родителей")

    class Meta:
        model = UnderageUserInfo
        fields = ("parental_confirm", "parental_phone", "parental_name", "cls_finished")
        widgets = {"cls_finished": forms.widgets.TextInput}

    def __init__(self, *args, **kwargs):
        super(UnderageUserInfoForm, self).__init__(*args, **kwargs)

        helper = FormHelper(self)
        helper.layout = Layout(
            Div(
                Field("parental_confirm"),
                Field("parental_phone"),
                Field("parental_name"),
                Field("cls_finished"),
            ),
            FormActions(Submit("submit", u"Отправить", css_class="btn btn-primary")),
        )
        self.helper = helper


class ApplicationCommentForm(forms.ModelForm):
    class Meta:
        model = ApplicationCaseComment
        fields = ("comment", "application")
        widgets = {"application": forms.HiddenInput}

    def __init__(self, *args, **kwargs):
        super(ApplicationCommentForm, self).__init__(*args, **kwargs)

        helper = FormHelper(self)
        helper.form_tag = False
        helper.layout = Layout(
            Field("application"),
            Div(Field("comment"), css_class="modal-body"),
            Div(
                FormActions(
                    HTML(
                        u'<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>'
                    ),
                    Submit("submit", u"Отправить", css_class="btn btn-primary"),
                ),
                css_class="modal-footer",
            ),
        )
        self.helper = helper


class AssignmentResponseForm(forms.ModelForm):
    class Meta:
        model = ApplicationAssignmentResponse
        fields = ("response", "assignment", "comment")
        widgets = {"assignment": forms.HiddenInput}

    def __init__(self, *args, **kwargs):
        super(AssignmentResponseForm, self).__init__(*args, **kwargs)

        helper = FormHelper(self)
        helper.layout = Layout(
            Field("response"),
            Field("comment"),
            Field("assignment"),
            FormActions(
                Submit("submit", u"Отправить", css_class="btn btn-primary"),
                HTML(
                    u"""
                     {% for url, caption in back_urls %}
                     <a class="btn btn-default" href="{{ url }}">{{ caption }}</a>
                     {% endfor %}
                     """
                ),
            ),
        )
        self.helper = helper


class AppEmailForm(forms.ModelForm):
    class Meta:
        model = AppEmail
        fields = ("subject", "text")

    def __init__(self, *args, **kwargs):
        super(AppEmailForm, self).__init__(*args, **kwargs)

        helper = FormHelper(self)
        helper.layout = Layout(
            Field("subject"),
            Field("text"),
            FormActions(
                Submit("submit", u"Отправить", css_class="btn btn-primary"),
                HTML(
                    u"""
                     {% for url, caption in back_urls %}
                     <a class="btn btn-default" href="{{ url }}">{{ caption }}</a>
                     {% endfor %}
                     """
                ),
            ),
        )
        self.helper = helper


class AppEmailWithCommentForm(forms.ModelForm):
    comment = forms.CharField(
        label=u"Комментарий",
        help_text=u"Опциональное замечание, для истории.",
        required=False,
        widget=forms.Textarea,
    )

    class Meta:
        model = AppEmail
        fields = ("subject", "text")

    def __init__(self, *args, **kwargs):
        super(AppEmailWithCommentForm, self).__init__(*args, **kwargs)

        helper = FormHelper(self)
        helper.layout = Layout(
            Field("subject"),
            Field("text"),
            HTML(
                u"""<h4>Комментарий &laquo;для своих&raquo; на анкете</h4>
                 <p>Это опциональное поле используется для внутренних заметок и для группы &laquo;Набор&raquo;.</p>
                 """
            ),
            Field("comment", rows=3),
            FormActions(
                Submit("submit", u"Отправить", css_class="btn btn-primary"),
                HTML(
                    u"""
                     {% for url, caption in back_urls %}
                     <a class="btn btn-default" href="{{ url }}">{{ caption }}</a>
                     {% endfor %}
                     """
                ),
            ),
        )
        self.helper = helper
