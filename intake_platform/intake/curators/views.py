import datetime
import json

# from django.contrib.auth.decorators import login_required
# from annoying.decorators import render_to
import os

from annoying.functions import get_config, get_object_or_None
from intake_platform.common.models import UserProfile
from intake_platform.common.tools import get_user, send_delayed_mails
from intake_platform.common.utils import UnicodeWriter
from django.contrib import messages
from django.core.mail import send_mail
from wsgiref.util import FileWrapper
from django.urls import reverse
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.template import Context, loader
from django.views import generic

from ..mixins import CuratorRequiredMixin
from ..forms import AppEmailWithCommentForm
from ..models import (
    ApplicationAssignmentResponse,
    ApplicationCase,
    UserAdditionalContacts,
    UserCuratorInfo,
    UserPhoto,
    UserPrepodInfo,
    WorkshopPetitionConnection,
    WorkshopProgramAssignment,
    WorkshopProgramEmailTemplate,
    WorkshopProgramMetaInfo,
    WorkshopProgramShoutOut,
)
from ..tools import (
    attach_buck,
    comment_on_case,
    detach_buck,
    generate_random_password,
    get_wp_accepted_apps,
    get_wp_counts_dict,
    get_wp_curators,
    get_wp_declined_apps,
    get_wp_passed_apps,
    get_wp_prepods,
    get_wp_retry_apps,
    has_buck,
    has_buck_after_buck,
    prepare_second_round,
    send_mail_to_applicant,
    workshop_assignments_for_curator,
)
from .forms import (
    AddCuratorForm,
    AddPrepodForm,
    CuratorAssignmentUpdateForm,
    CuratorEmailTemplateForm,
    ShoutOutForm,
)

# from django.template import RequestContext


class CuratorControlPanel(CuratorRequiredMixin, generic.TemplateView):
    template_name = "intake/curators/cp.html"

    def get_context_data(self, **kwargs):
        context = super(CuratorControlPanel, self).get_context_data(**kwargs)

        # URL = 'https://letnyayashkola.org/api/v1.0/workshops/'
        # context['workshops'] = requests.get(URL).json()

        u = get_user(self.request)
        prof = u.userprofile
        context["profile"] = prof

        context["this_user_workshop_programs"] = prof.workshop_programs(
            include_apps=True
        )

        return context


class CuratorAppDetails(CuratorRequiredMixin, generic.DetailView):
    model = ApplicationCase
    template_name = "intake/curators/app_details.html"

    def get_context_data(self, **kwargs):
        context = super(CuratorAppDetails, self).get_context_data(**kwargs)

        u = get_user(self.request)
        if not u.userprofile.is_curator_for_app(pk=self.kwargs["pk"]):
            return redirect("profile")

        prof = u.userprofile
        context["profile"] = prof
        return context


class CuratorWPAppsList(CuratorRequiredMixin, generic.ListView):
    template_name = "intake/curators/apps_list.html"
    context_object_name = "applications"
    paginate_by = 10

    def apps_qs(self):
        w_slug = self.kwargs["w_slug"]
        p_slug = self.kwargs["p_slug"]
        return get_wp_passed_apps(w_slug, p_slug)

    def get_queryset(self):
        w_slug = self.kwargs["w_slug"]
        p_slug = self.kwargs["p_slug"]
        u = get_user(self.request)

        if not u.userprofile.is_curator_for(w_slug, p_slug):
            return redirect("profile")

        qs = self.apps_qs()

        return qs

    def get_context_data(self, **kwargs):
        w_slug = self.kwargs["w_slug"]
        p_slug = self.kwargs["p_slug"]
        w_meta = get_object_or_404(
            WorkshopProgramMetaInfo, workshop_slug=w_slug, program_slug=p_slug
        )

        context = super(CuratorWPAppsList, self).get_context_data(**kwargs)

        u = get_user(self.request)
        prof = u.userprofile
        context["profile"] = prof
        context["workshop_name"] = w_meta.workshop_name
        context["program_name"] = w_meta.program_name
        context["workshop_slug"] = w_meta.workshop_slug
        context["program_slug"] = w_meta.program_slug
        context["wp_has_assignment"] = WorkshopProgramAssignment.objects.filter(
            workshop_program=w_meta
        ).exists()
        context["wp_counts"] = get_wp_counts_dict(w_slug, p_slug)

        return context


class CuratorWPAppsListHideSlackers(CuratorWPAppsList):
    template_name = "intake/curators/apps_list_hide_slackers.html"

    def apps_qs(self):
        w_slug = self.kwargs["w_slug"]
        p_slug = self.kwargs["p_slug"]
        apps = get_wp_passed_apps(w_slug, p_slug)
        nu_aps = filter(
            lambda x: x.has_complete_homework(), get_wp_passed_apps(w_slug, p_slug)
        )
        return nu_aps


class CuratorAppsListTables(CuratorRequiredMixin, generic.ListView):
    template_name = "intake/curators/apps_list_tables.html"
    context_object_name = "applications"

    def get_queryset(self):
        u = get_user(self.request)
        from intake_platform.intake import tools as int_too

        qs = []
        for v in u.userprofile.workshop_programs().itervalues():
            w_slug, p_slug = v["workshop_slug"], v["program_slug"]
            if not u.userprofile.is_curator_for(w_slug, p_slug):
                return redirect("profile")

            qs += list(
                int_too._get_wp_apps_filtered(w_slug, p_slug, int_too._passed).order_by(
                    "-created"
                )
            )

        return qs


class RetryAppsList(CuratorRequiredMixin, generic.ListView):
    template_name = "intake/curators/retry_apps_list.html"
    context_object_name = "applications"
    paginate_by = 10

    def apps_qs(self):
        # FIXME: FINISH ME
        w_slug = self.kwargs["w_slug"]
        p_slug = self.kwargs["p_slug"]
        return get_wp_retry_apps(w_slug, p_slug)

    def get_queryset(self):
        w_slug = self.kwargs["w_slug"]
        p_slug = self.kwargs["p_slug"]
        u = get_user(self.request)

        if not u.userprofile.is_curator_for(w_slug, p_slug):
            return redirect("profile")

        qs = self.apps_qs()

        return qs

    def get_context_data(self, **kwargs):
        w_slug = self.kwargs["w_slug"]
        p_slug = self.kwargs["p_slug"]
        w_meta = get_object_or_404(
            WorkshopProgramMetaInfo, workshop_slug=w_slug, program_slug=p_slug
        )

        context = super(RetryAppsList, self).get_context_data(**kwargs)

        u = get_user(self.request)
        prof = u.userprofile
        context["profile"] = prof
        context["workshop_name"] = w_meta.workshop_name
        context["program_name"] = w_meta.program_name
        context["workshop_slug"] = w_meta.workshop_slug
        context["program_slug"] = w_meta.program_slug
        context["wp_has_assignment"] = WorkshopProgramAssignment.objects.filter(
            workshop_program=w_meta
        ).exists()
        context["wp_counts"] = get_wp_counts_dict(w_slug, p_slug)

        return context


class AcceptedAppsList(CuratorWPAppsList):
    template_name = "intake/curators/accepted_apps_list.html"

    def apps_qs(self):
        w_slug = self.kwargs["w_slug"]
        p_slug = self.kwargs["p_slug"]
        return get_wp_accepted_apps(w_slug, p_slug)


class AcceptedAppsCSV(CuratorRequiredMixin, generic.View):
    fname_prefix = "report_accepted_apps"

    def get_apps(self, w_slug, p_slug):
        return get_wp_accepted_apps(w_slug, p_slug)

    def get_filename(self, w_slug, p_slug):
        now_str = datetime.datetime.now().strftime("%Y%b%d").lower()
        return "%s_%s-%s_%s.csv" % (self.fname_prefix, w_slug, p_slug, now_str)

    def get(self, request, *args, **kwargs):
        w_slug = self.kwargs["w_slug"]
        p_slug = self.kwargs["p_slug"]

        from cStringIO import StringIO

        csvfile = StringIO()
        csvwriter = UnicodeWriter(csvfile, encoding="cp1251")

        def read_and_flush(csvfile):
            csvfile.seek(0)
            data = csvfile.read()
            csvfile.seek(0)
            csvfile.truncate()
            return data

        def data():
            for app in self.get_apps(w_slug, p_slug):
                csvwriter.writerow([u"%s" % f for (_, f) in app.csv_fields()])

                data = read_and_flush(csvfile)
                yield data

        response = HttpResponse(data())
        fname = self.get_filename(w_slug, p_slug)
        response["Content-Disposition"] = "attachment; filename=%s" % fname
        response["mimetype"] = "text/csv"
        return response


class AcceptedAppsExcel(CuratorRequiredMixin, generic.View):
    fname_prefix = "report_accepted_apps"

    def get_apps(self, w_slug, p_slug):
        return get_wp_accepted_apps(w_slug, p_slug)

    def get_filename(self, w_slug, p_slug):
        now_str = datetime.datetime.now().strftime("%Y%b%d").lower()
        return "%s_%s-%s_%s.xlsx" % (self.fname_prefix, w_slug, p_slug, now_str)

    def get(self, request, *args, **kwargs):
        w_slug = self.kwargs["w_slug"]
        p_slug = self.kwargs["p_slug"]

        import openpyxl
        from openpyxl.cell import get_column_letter

        fname = self.get_filename(w_slug, p_slug)
        response = HttpResponse()
        response[
            "mimetype"
        ] = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        response["Content-Disposition"] = "attachment; filename=%s" % fname

        workbook = openpyxl.Workbook()  # outfile, {'in_memory': True})
        worksheet = workbook.get_active_sheet()

        # TODO: refactor!
        # TODO: add header
        # columns = [x for x in ApplicationCase.export_fields().items()]

        # for col_num, col_data in enumerate(columns, start=1):
        #   #c = worksheet.cell(row=1, column=col_num)
        #   #name, width = col_data
        #   #c.value = name
        #   #c.style.font.bold = True
        #   #column_letter = get_column_letter(col_num)
        #   #worksheet.column_dimensions[column_letter].width = width

        for col_num in [1, 2, 3, 4, 6, 7]:
            c = worksheet.cell(row=1, column=col_num)
            c.style.font.bold = True
            column_letter = get_column_letter(col_num)
            worksheet.column_dimensions[column_letter].width = 20

        col_num = 5  # Location column
        c = worksheet.cell(row=1, column=col_num)
        c.style.font.bold = True
        column_letter = get_column_letter(col_num)
        worksheet.column_dimensions[column_letter].width = 80

        for i, app in enumerate(self.get_apps(w_slug, p_slug), start=1):
            row = [val for (_, val, _) in app.xsl_fields()]
            for col_num in xrange(len(row)):
                c = worksheet.cell(row=i, column=col_num + 1)
                c.value = row[col_num]
                c.style.alignment.wrap_text = True

            # for j, f in enumerate([f for (_, f) in app.csv_fields()]):
            # worksheet.write(i, j, u"%s" % f)

        workbook.save(response)
        return response


class PassedAppsCSV(AcceptedAppsCSV):
    fname_prefix = "report_passed_apps"

    def get_apps(self, w_slug, p_slug):
        return get_wp_passed_apps(w_slug, p_slug)


class PassedAppsExcel(AcceptedAppsExcel):
    fname_prefix = "report_passed_apps"

    def get_apps(self, w_slug, p_slug):
        return get_wp_passed_apps(w_slug, p_slug)


class DeclinedAppsList(CuratorWPAppsList):
    template_name = "intake/curators/declined_apps_list.html"

    def apps_qs(self):
        w_slug = self.kwargs["w_slug"]
        p_slug = self.kwargs["p_slug"]
        return get_wp_declined_apps(w_slug, p_slug)


class CuratorEmailTemplatesControl(CuratorRequiredMixin, generic.View):
    template_name = "intake/curators/email_templates_control.html"
    form_class = CuratorEmailTemplateForm
    ext_whitelist = (".doc", ".docx", ".zip", ".pdf")

    def get(self, request, *args, **kwargs):
        user = get_user(request)
        w_metas = user.workshopprogrammetainfo_set.all()
        wp_templates = WorkshopProgramEmailTemplate.all_for_wp_curator(user)

        form = self.form_class()
        form.fields["workshop_program"].choices = [
            (wp.pk, u"%s: %s" % (wp.workshop_name, wp.program_name)) for wp in w_metas
        ]

        return render(
            request,
            self.template_name,
            {"form": form, "w_metas": w_metas, "wp_templates": wp_templates},
        )

    def post(self, request, *args, **kwargs):
        user = get_user(request)
        w_metas = user.workshopprogrammetainfo_set.all()
        wp_templates = WorkshopProgramEmailTemplate.all_for_wp_curator(user)

        # TODO: check that user не пытается править чужой темплейт

        form = self.form_class(request.POST)
        form.fields["workshop_program"].choices = [
            (wp.pk, u"%s: %s" % (wp.workshop_name, wp.program_name)) for wp in w_metas
        ]
        if form.is_valid():
            tmpl = form.save()
            tmpl.save()
            messages.success(request, u"Готово. Новый шаблон сохранён")
            return redirect("curators:email_templates_control")

        return render(
            request,
            self.template_name,
            {"form": form, "w_metas": w_metas, "wp_templates": wp_templates},
        )


class CuratorHomeworkControl(CuratorRequiredMixin, generic.View):
    template_name = "intake/curators/homework_control.html"
    form_class = CuratorAssignmentUpdateForm
    ext_whitelist = (".doc", ".docx", ".zip", ".pdf", ".xlsx", ".xls")
    # template_name = "intake/assignments/fill_in.html"

    def get(self, request, *args, **kwargs):
        # hw = u.applicationassignmentresponse
        user = get_user(request)
        w_metas = user.workshopprogrammetainfo_set.all()
        w_homeworks = workshop_assignments_for_curator(user)

        form = self.form_class()
        form.fields["workshop_program"].choices = [
            (wp.pk, u"%s: %s" % (wp.workshop_name, wp.program_name)) for wp in w_metas
        ]

        return render(
            request,
            self.template_name,
            {"form": form, "w_metas": w_metas, "w_homeworks": w_homeworks},
        )

    def post(self, request, *args, **kwargs):
        user = get_user(request)
        w_metas = user.workshopprogrammetainfo_set.all()
        w_homeworks = workshop_assignments_for_curator(user)

        # TODO: check that user не мудило ёбаное, и не пытается залить чужой
        # ассайнмент сучара

        form = self.form_class(request.POST, request.FILES)
        form.fields["workshop_program"].choices = [
            (wp.pk, u"%s: %s" % (wp.workshop_name, wp.program_name)) for wp in w_metas
        ]
        if form.is_valid():
            data = form.cleaned_data["assignment"]
            filename = data.name
            ext = os.path.splitext(filename)[1]
            ext = ext.lower()
            if ext not in self.ext_whitelist:
                messages.error(
                    request,
                    f"Запрещённый тип документа! Только {', '.join(self.ext_whitelist)}, никаких {ext}"
                )
            else:
                hw = form.save()
                hw.save()
                messages.success(request, u"Побйеда. Обновили %s" % hw)
                return redirect("curators:homework_control")

        return render(
            request,
            self.template_name,
            {"form": form, "w_metas": w_metas, "w_homeworks": w_homeworks},
        )


class SendHomework(CuratorRequiredMixin, generic.View):
    def get(self, request, *args, **kwargs):
        u = get_user(request)
        app = get_object_or_404(ApplicationCase, pk=self.kwargs["pk"])
        if not u.userprofile.is_curator_for_app(pk=app.pk):
            messages.error(
                request,
                u"%s -- не твоя, вот и бесишься? Ты чё-то попутал, дружище." % app,
            )
            messages.error(request, u"Может ты вообще не куратор?")
            return redirect("profile")

        if prepare_second_round(
            app, intention="set by {}".format(u.username), automatic=False
        ):
            subject = u"ЛШ2020: вступительное задание"
            text = u"""Добрый день.

Вам предлагается выполнить вступительное задание для выбранной программы.

Чтобы это сделать, необходимо войти на сайт https://nabor.letnyayashkola.org и проследовать указанным там инструкциям.

Всего доброго
Сара Коннор
"""
            send_mail_to_applicant(app, subject, text)

            messages.success(request, u"%s: Отправлено вступительное задание" % app)
        else:
            messages.error(
                request,
                u"%s (pk#%s): вступительное задание отправить не получилось. Или оно уже было отправлено, или что-то случилось посерьёзней."
                % (app, app.pk),
            )

        w_slug, p_slug = app.wp_slug_tuple
        return redirect("curators:apps_list", w_slug, p_slug)


class InspectHomework(CuratorRequiredMixin, generic.View):
    template_name = "intake/curators/app_inspect_homework.html"

    def get(self, request, *args, **kwargs):
        u = get_user(request)
        app = get_object_or_None(ApplicationCase, pk=self.kwargs["app_pk"])
        hw = get_object_or_None(
            ApplicationAssignmentResponse, pk=self.kwargs["ass_pk"], user=app.user
        )
        if hw is None or app is None:
            messages.error(
                request, u"Не могу найти такую анкету или вступительное задание"
            )
            return redirect("profile")
        if not u.userprofile.is_curator_for_app(pk=app.pk):
            messages.error(
                request,
                u"%s -- не твоя, вот и бесишься? Ты чё-то попутал, дружище." % app,
            )
            messages.error(request, u"Может ты вообще не куратор?")
            return redirect("profile")
        if not hw.is_complete:
            messages.error(request, u"Это задание не завершено.")
            return redirect("profile")

        return render(request, self.template_name, {"application": app, "homework": hw})


class DownloadAppHomework(CuratorRequiredMixin, generic.View):
    def get_filename(self, username, user_email_tail, w_slug, p_slug):
        return u"otvet_%s_%s_2020_%s_%s" % (username, user_email_tail, w_slug, p_slug)

    def get(self, request, *args, **kwargs):
        u = get_user(request)
        app = get_object_or_None(ApplicationCase, pk=self.kwargs["app_pk"])
        hw = get_object_or_None(
            ApplicationAssignmentResponse, pk=self.kwargs["ass_pk"], user=app.user
        )
        if hw is None or app is None:
            messages.error(
                request, u"Не могу найти такую анкету или вступительное задание"
            )
            return redirect("profile")
        if not u.userprofile.is_curator_for_app(pk=app.pk):
            messages.error(
                request,
                u"%s -- не твоя, вот и бесишься? Ты чё-то попутал, дружище." % app,
            )
            messages.error(request, u"Может ты вообще не куратор?")
            return redirect("profile")
        if not hw.is_complete:
            messages.error(request, u"Это задание не завершено.")
            return redirect("profile")

        ass = hw.response
        filepath = ass.path

        wpc = app.user.workshoppetitionconnection
        fname = self.get_filename(
            app.user.email.split("@")[0],
            app.user.email.split("@")[1],
            wpc.workshop_slug,
            wpc.program_slug,
        )
        ext = os.path.splitext(filepath)[1]

        wrapper = FileWrapper(file(filepath))
        response = HttpResponse(wrapper, content_type="text/plain")
        response["Content-Disposition"] = u"attachment; filename=%s%s" % (fname, ext)
        response["Content-Length"] = ass.size
        if ext == ".docx":
            response[
                "mimetype"
            ] = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
        elif ext == ".zip":
            response["mimetype"] = "application/zip"
        elif ext == ".pdf":
            response["mimetype"] = "application/pdf"
        else:
            response["mimetype"] = "application/msword"

        return response


class CuratorShoutOut(CuratorRequiredMixin, generic.View):
    template_name = "intake/curators/shout_out.html"
    email_subject = u"[ЛШ2020] Сообщение от кураторов"
    email_template_name = "intake/curators/emails/shout_out.txt"
    shout_type = WorkshopProgramShoutOut.TO_PASSED_APPS
    form_class = ShoutOutForm

    def get_apps_qs(self, workshop_slug, program_slug):
        # FIXME: check if this is not empty?
        wp_apps = get_wp_passed_apps(workshop_slug, program_slug)
        return wp_apps

    def get(self, request, *args, **kwargs):
        user = get_user(request)
        selected_wp = get_object_or_404(
            WorkshopProgramMetaInfo,
            workshop_slug=kwargs["w_slug"],
            program_slug=kwargs["p_slug"],
        )
        w_metas = user.workshopprogrammetainfo_set.all()

        form = self.form_class()
        form.fields["workshop_program"].choices = [
            (
                wp.pk,
                u"Участникам программы %s: %s" % (wp.workshop_name, wp.program_name),
            )
            for wp in w_metas
        ]
        form.fields["subject"].initial = self.email_subject
        t = loader.get_template(self.email_template_name)
        form.fields["text"].initial = t.render(
            Context({"user_profile": user.userprofile, "w_meta": w_metas})
        )
        form.fields["workshop_program"].initial = selected_wp.pk

        return render(request, self.template_name, {"form": form, "w_metas": w_metas})

    def post(self, request, *args, **kwargs):
        user = get_user(request)
        w_metas = user.workshopprogrammetainfo_set.all()

        form = self.form_class(request.POST)
        form.fields["workshop_program"].choices = [
            (
                wp.pk,
                u"Участникам программы %s: %s" % (wp.workshop_name, wp.program_name),
            )
            for wp in w_metas
        ]
        if form.is_valid():
            shout = form.save(commit=False)
            shout.author = user
            shout.shout_type = self.shout_type
            w_s = shout.workshop_program.workshop_slug
            p_s = shout.workshop_program.program_slug
            wp_apps = self.get_apps_qs(w_s, p_s)
            emails = [a.user.email for a in wp_apps]
            shout.save()
            shout.users = [a.user for a in wp_apps]
            send_delayed_mails(emails, shout.subject, shout.text)
            messages.success(
                request,
                u"Ушло массовое сообщение всем участникам %s" % shout.workshop_program,
            )
            return redirect("profile")

        return render(request, self.template_name, {"form": form, "w_metas": w_metas})


class CuratorShoutOutWPAccepted(CuratorShoutOut):
    template_name = "intake/curators/shout_out_wp_accepted.html"
    email_subject = u"[ЛШ2020] Сообщение принятым участникам от кураторов"
    email_template_name = "intake/curators/emails/shout_out_wp_accepted.txt"
    shout_type = WorkshopProgramShoutOut.TO_ACCEPTED_APPS

    def get_apps_qs(self, workshop_slug, program_slug):
        return get_wp_accepted_apps(workshop_slug, program_slug)


class CuratorShoutOutWPDeclined(CuratorShoutOut):
    template_name = "intake/curators/shout_out_wp_declined.html"
    email_subject = u"[ЛШ2020] Сообщение отказанным участникам от кураторов"
    email_template_name = "intake/curators/emails/shout_out_wp_declined.txt"
    shout_type = WorkshopProgramShoutOut.TO_DECLINED_APPS

    def get_apps_qs(self, workshop_slug, program_slug):
        return get_wp_declined_apps(workshop_slug, program_slug)


class CuratorShoutOutWPSlackers(CuratorShoutOut):
    template_name = "intake/curators/shout_out_wp_slackers.html"
    email_subject = u"[ЛШ2020] Не можем найти ваше вступительное задание"
    email_template_name = "intake/curators/emails/shout_out_wp_slackers.txt"
    shout_type = WorkshopProgramShoutOut.TO_SLACKERS_APPS

    def get_apps_qs(self, w_slug, p_slug):
        apps = get_wp_passed_apps(w_slug, p_slug)
        nu_apps = filter(
            lambda x: x.has_complete_homework(), get_wp_passed_apps(w_slug, p_slug)
        )
        return [a for a in apps if a not in nu_apps]


class CuratorAcceptApp(CuratorRequiredMixin, generic.View):
    def post(self, request, *args, **kwargs):
        u = get_user(request)
        back_url = request.POST.get("back_url", "profile")

        a_pk = request.POST.get("a_pk", None)
        if a_pk is None:
            messages.error(request, u"Заявку не удалось принять. Напишите Саре.")
            return redirect(back_url)

        app = get_object_or_404(ApplicationCase, pk=a_pk)
        w_slug, p_slug = app.wp_slug_tuple

        if u.userprofile.is_castrated:
            messages.error(request, u"У вас отключен доступ к этой операции")
            return redirect("profile")

        if not u.userprofile.is_curator_for_app(pk=a_pk):
            messages.error(
                request, u"Ты чё-то попутал, дружище. %s -- не из твоих" % app
            )
            messages.error(request, u"Может ты вообще не куратор?")
            return redirect("profile")

        if attach_buck(app.user, "accepted_by_workshop"):
            messages.success(
                request, u"%s (#%s) теперь 'принята на ЛШ'." % (app, app.pk)
            )

            subject = u"ЛШ2020: Вас берут"
            tmpl = WorkshopProgramEmailTemplate.get_after_wp_accept_for_app(app)
            text = ""
            if tmpl is not None:
                text = tmpl.render()
                if len(text) > 0:
                    attach_buck(app.user, "email_template_wp_accepted_auto_sentt")

            text = (
                text
                if len(text) > 0
                else u"""Добрый день.

Поздравляю, вы приняты на ЛШ2020. Мы вас посчитали, и ждём, что вы к нам приедете.

До встречи на ЛШ.

Не забывайте время от времени заходить на https://nabor.letnyayashkola.org, мы можем иногда запрашивать дополнительную справочную информацию.

Всякие новости можно найти на https://letnyayashkola.org и в нашей группе вконтакте https://vk.com/shkola_rr

Всего доброго
Сара Коннор
"""
            )
            send_mail_to_applicant(app, subject, text)

            messages.success(request, u"Пользователю направлено письмо счастья.")

            comment_on_case(
                case=app,
                author=u,
                comment=u"[auto] Заявка принята на {}/{}.".format(w_slug, p_slug),
            )
        else:
            messages.error(
                request,
                u"Weirdest shit happens. Анкета %s (#%s) уже была принятой."
                % (app, app.pk),
            )

        return redirect(back_url)


class CuratorAppMarkSonOfABitch(CuratorRequiredMixin, generic.View):
    def post(self, request, *args, **kwargs):
        u = get_user(request)
        back_url = request.POST.get("back_url")

        a_pk = self.kwargs["app_pk"]
        if a_pk is None:
            messages.error(request, u"Заявку не удалось пометить. Напишите Саре.")
            return redirect("profile")

        app = get_object_or_404(ApplicationCase, pk=a_pk)
        w_slug, p_slug = app.wp_slug_tuple

        if back_url is None:
            retval = redirect(
                "curators:accepted_apps_list", w_slug=w_slug, p_slug=p_slug
            )
        else:
            retval = redirect(back_url)

        if not u.userprofile.is_curator_for_app(pk=a_pk):
            messages.error(
                request, u"Ты чё-то попутал, дружище. %s -- не из твоих" % app
            )
            messages.error(request, u"Может ты вообще не куратор?")
            return redirect("profile")

        if attach_buck(app.user, "son_of_a_bitch"):
            messages.success(
                request, u"%s (#%s) теперь 'Не доехал до ЛШ'." % (app, app.pk)
            )
        else:
            messages.error(
                request,
                u"Weirdest shit happens. Анкета %s (#%s) уже была помечена недоехавшей."
                % (app, app.pk),
            )

        return retval


class CuratorAppDemarkSonOfABitch(CuratorRequiredMixin, generic.View):
    def get(self, request, *args, **kwargs):
        u = get_user(request)

        a_pk = self.kwargs["app_pk"]
        if a_pk is None:
            messages.error(request, u"Заявку не удалось пометить. Напишите Саре.")
            return redirect("profile")

        app = get_object_or_404(ApplicationCase, pk=a_pk)
        w_slug, p_slug = app.wp_slug_tuple

        retval = redirect("curators:accepted_apps_list", w_slug=w_slug, p_slug=p_slug)

        if not u.userprofile.is_curator_for_app(pk=a_pk):
            messages.error(
                request, u"Ты чё-то попутал, дружище. %s -- не из твоих" % app
            )
            messages.error(request, u"Может ты вообще не куратор?")
            return redirect("profile")

        if detach_buck(app.user, "son_of_a_bitch"):
            messages.success(
                request, u"%s (#%s) Метка 'не доехал до ЛШ' снята." % (app, app.pk)
            )
        else:
            messages.error(
                request,
                u"Weirdest shit happens. Анкета %s (#%s) уже была помечена недоехавшей."
                % (app, app.pk),
            )

        return retval


class CuratorDeclineApp(CuratorRequiredMixin, generic.View):
    template_name = "intake/curators/app_decline.html"
    email_template = "intake/curators/emails/app_decline.txt"
    form_class = AppEmailWithCommentForm

    def __init__(self, *args, **kwargs):
        super(CuratorDeclineApp, self).__init__(*args, **kwargs)

    def back_url(self, w_slug, p_slug):
        return reverse(
            "curators:apps_list", kwargs={"w_slug": w_slug, "p_slug": p_slug}
        )

    def allowed_to_alter_this_app(self, request, app):
        prof = get_user(request).userprofile
        if prof.is_castrated:
            return False
        allowed = prof.is_curator_for_app(pk=app.pk)
        if not allowed:
            messages.error(
                request,
                u"{a} (#{a.pk}) -- не из твоих. Ты вообще куратор?".format(a=app),
            )

        return allowed

    def get(self, request, *args, **kwargs):
        app = get_object_or_404(ApplicationCase, pk=self.kwargs["pk"])
        if not self.allowed_to_alter_this_app(request, app):
            return redirect("profile")

        w_slug, p_slug = app.wp_slug_tuple
        w_name, p_name = app.wp_names_tuple

        if has_buck(app.user, "declined_by_workshop"):
            if not has_buck_after_buck(
                app.user, "retry_granted", "declined_by_workshop"
            ):
                messages.error(request, u"%s уже помечена как отклонённая" % app)
                return redirect(self.back_url(w_slug, p_slug))

        form = self.form_class()
        form.fields[
            "subject"
        ].initial = u"[ЛШ2020] Отказ от участия в программе {}/{}".format(
            w_name, p_name
        )
        t = loader.get_template(self.email_template)
        form.fields["text"].initial = t.render(
            Context(
                {
                    "user_profile": get_user(request).userprofile,
                    "w_name": w_name,
                    "p_name": p_name,
                }
            )
        )

        return render(
            request,
            self.template_name,
            {
                "form": form,
                "application": app,
                "back_urls": [(self.back_url(w_slug, p_slug), u"Назад")],
            },
        )

    def post(self, request, *args, **kwargs):
        app = get_object_or_404(ApplicationCase, pk=self.kwargs["pk"])
        if not self.allowed_to_alter_this_app(request, app):
            return redirect("profile")

        w_slug, p_slug = app.wp_slug_tuple

        form = self.form_class(request.POST)
        if form.is_valid():
            b = attach_buck(app.user, "declined_by_workshop", get_buck=True)
            if b is not None:
                messages.success(
                    request, u"%s (#%s) теперь 'отклонена мастерской'" % (app, app.pk)
                )

                mejl = form.save(commit=False)
                mejl.author = get_user(request)
                mejl.to_user = app.user
                mejl.relevant_buck = b
                mejl.save()

                if send_mail_to_applicant(app, mejl.subject, mejl.text):
                    messages.success(request, u"Пользователь уведомлён.")
                else:
                    messages.error(
                        request,
                        u"Не получилось отправить пользователю письмо, проблемы с сервером.",
                    )

                messages.info(
                    request,
                    u"В таймлайне пользователя появится информация, дублирующая отправленное письмо.",
                )

                comment_on_case(
                    case=app,
                    author=mejl.author,
                    comment=u"[auto] Заявку не хотят на {}/{}.".format(w_slug, p_slug),
                )
                comment = form.cleaned_data.get("comment")
                if len(comment) > 0:
                    comment_on_case(case=app, author=mejl.author, comment=comment)
            else:
                messages.error(
                    request, u"Не получилось отказать этой заявке. Попробуй ещё раз."
                )

            return redirect("curators:apps_list", w_slug, p_slug)

        return render(
            request,
            self.template_name,
            {
                "form": form,
                "application": app,
                "back_urls": [(self.back_url(w_slug, p_slug), u"Назад")],
            },
        )


class CuratorDeclineAppBecauseSlow(CuratorDeclineApp):
    email_template = "intake/curators/emails/app_decline_slow.txt"


class CuratorPrepods(CuratorRequiredMixin, generic.View):
    """ FIXME: Not sure why I need this one """

    template_name = "intake/curators/apps_list.html"
    context_object_name = "applications"
    paginate_by = 10


class EldersCuratorsList(CuratorRequiredMixin, generic.ListView):
    template_name = "intake/curators/elders/curators_list.html"
    context_object_name = "curators"
    paginate_by = 10

    def get_queryset(self):
        w_slug = self.kwargs["w_slug"]
        p_slug = self.kwargs["p_slug"]
        print(w_slug)
        u = get_user(self.request)

        if not u.userprofile.is_curator_for(w_slug, p_slug):
            return redirect("profile")

        qs = get_wp_prepods(w_slug, p_slug)

        return qs

    def get_context_data(self, **kwargs):
        w_slug = self.kwargs["w_slug"]
        p_slug = self.kwargs["p_slug"]
        w_meta = get_object_or_404(
            WorkshopProgramMetaInfo, workshop_slug=w_slug, program_slug=p_slug
        )

        context = super(EldersCuratorsList, self).get_context_data(**kwargs)

        u = get_user(self.request)
        prof = u.userprofile
        context["profile"] = prof
        context["workshop_name"] = w_meta.workshop_name
        context["program_name"] = w_meta.program_name
        context["workshop_slug"] = w_meta.workshop_slug
        context["program_slug"] = w_meta.program_slug
        context["wp_has_assignment"] = WorkshopProgramAssignment.objects.filter(
            workshop_program=w_meta
        ).exists()
        context["wp_counts"] = get_wp_counts_dict(w_slug, p_slug)

        # UGLY
        from intake_platform.intake.models import ApplicationCase

        context["a"] = ApplicationCase.objects.get(pk=20)

        return context


class CuratorPrepodAdd(CuratorRequiredMixin, generic.View):
    template_name = "intake/curators/elders/prepod_add.html"
    form_class = AddPrepodForm

    def back_url(self, w_slug, p_slug):
        return reverse(
            "curators:prepods_list", kwargs={"w_slug": w_slug, "p_slug": p_slug}
        )

    def get(self, request, *args, **kwargs):
        user = get_user(request)
        w_slug, p_slug = kwargs["w_slug"], kwargs["p_slug"]

        w_metas = user.workshopprogrammetainfo_set.all()

        if not w_metas.filter(workshop_slug=w_slug, program_slug=p_slug).exists():
            messages.error(
                request, u"? Вы пытаетесь сделать что-то нехорошое. Не надо так"
            )
            return redirect("profile")
        form = self.form_class()
        form.fields["workshop_program"].choices = [
            (wp.pk, u"%s: %s" % (wp.workshop_name, wp.program_name)) for wp in w_metas
        ]

        form.fields["workshop_program"].initial = w_metas.get(
            workshop_slug=w_slug, program_slug=p_slug
        )

        return render(
            request,
            self.template_name,
            {
                "form": form,
                "workshop_slug": w_slug,
                "program_slug": p_slug,
                "back_urls": [
                    (self.back_url(w_slug, p_slug), u"Назад к списку преподов")
                ],
            },
        )

    def post(self, request, *args, **kwargs):
        user = get_user(request)
        w_slug, p_slug = kwargs["w_slug"], kwargs["p_slug"]

        w_metas = user.workshopprogrammetainfo_set.all()
        if not w_metas.filter(workshop_slug=w_slug, program_slug=p_slug).exists():
            messages.error(
                request, u"? Вы пытаетесь сделать что-то нехорошое. Не надо так"
            )
            return redirect("profile")

        context = {}
        context["workshop_slug"] = w_slug
        context["program_slug"] = p_slug

        context["back_urls"] = [
            (self.back_url(w_slug, p_slug), u"Назад к списку преподов")
        ]

        form = self.form_class(request.POST, request.FILES)
        form.fields["workshop_program"].choices = [
            (wp.pk, u"%s: %s" % (wp.workshop_name, wp.program_name)) for wp in w_metas
        ]

        if "lectures" in form.data:
            context["lectures_data"] = json.loads(form.data["lectures"])

        if form.is_valid():
            messages.error(
                request,
                u"Неподдерживаемый функционал."
            )
        else:
            messages.error(request, u"Форма заполнена неполностью и/или с ошибками")

        context["form"] = form
        return render(request, self.template_name, context)


class CuratorPrepodDelete(CuratorRequiredMixin, generic.View):
    def back_url(self, w_slug, p_slug):
        return reverse(
            "curators:prepods_list", kwargs={"w_slug": w_slug, "p_slug": p_slug}
        )

    def post(self, request, *args, **kwargs):
        w_slug = self.kwargs["w_slug"]
        p_slug = self.kwargs["p_slug"]
        user = get_user(self.request)

        back_url = self.back_url(w_slug, p_slug)
        success_url = back_url

        w_metas = user.workshopprogrammetainfo_set.all()
        if not w_metas.filter(workshop_slug=w_slug, program_slug=p_slug).exists():
            messages.error(
                request, u"? Вы пытаетесь сделать что-то нехорошое. Не надо так"
            )
            return redirect(back_url)

        prep = get_object_or_None(
            UserPrepodInfo,
            pk=self.kwargs["prepod_pk"],
            workshop_program__workshop_slug=w_slug,
            workshop_program__program_slug=p_slug,
        )

        if prep is None:
            messages.error(
                request,
                u"? Или у вас не хватает прав для этого действия, или такого препода нет",
            )
            return redirect(back_url)

        prep.user.delete()
        messages.success(request, u"Препод удалён")

        return redirect(success_url)


class CuratorPrepodsList(CuratorRequiredMixin, generic.ListView):
    template_name = "intake/curators/elders/prepods_list.html"
    context_object_name = "prepods"
    paginate_by = 10

    def get_queryset(self):
        w_slug = self.kwargs["w_slug"]
        p_slug = self.kwargs["p_slug"]
        u = get_user(self.request)

        if not u.userprofile.is_curator_for(w_slug, p_slug):
            return redirect("profile")

        qs = get_wp_prepods(w_slug, p_slug)

        return qs

    def get_context_data(self, **kwargs):
        w_slug = self.kwargs["w_slug"]
        p_slug = self.kwargs["p_slug"]
        w_meta = get_object_or_404(
            WorkshopProgramMetaInfo, workshop_slug=w_slug, program_slug=p_slug
        )

        context = super(CuratorPrepodsList, self).get_context_data(**kwargs)

        u = get_user(self.request)
        prof = u.userprofile
        context["profile"] = prof
        context["workshop_name"] = w_meta.workshop_name
        context["program_name"] = w_meta.program_name
        context["workshop_slug"] = w_meta.workshop_slug
        context["program_slug"] = w_meta.program_slug
        context["wp_counts"] = get_wp_counts_dict(w_slug, p_slug)

        return context


class CuratorCuratorAdd(CuratorRequiredMixin, generic.View):
    template_name = "intake/curators/elders/curator_add.html"
    form_class = AddCuratorForm

    def back_url(self, w_slug, p_slug):
        return reverse(
            "curators:curators_list", kwargs={"w_slug": w_slug, "p_slug": p_slug}
        )

    def get(self, request, *args, **kwargs):
        user = get_user(request)
        w_slug, p_slug = kwargs["w_slug"], kwargs["p_slug"]
        if not user.userprofile.is_beta_tester:
            messages.error(
                request, u"Эта функция недоступна непродвинутым пользователям."
            )
            return redirect("profile")

        w_metas = user.workshopprogrammetainfo_set.all()

        if not w_metas.filter(workshop_slug=w_slug, program_slug=p_slug).exists():
            messages.error(
                request, u"? Вы пытаетесь сделать что-то нехорошое. Не надо так"
            )
            return redirect("profile")
        form = self.form_class()
        form.fields["workshop_program"].choices = [
            (wp.pk, u"%s: %s" % (wp.workshop_name, wp.program_name)) for wp in w_metas
        ]

        form.fields["workshop_program"].initial = w_metas.get(
            workshop_slug=w_slug, program_slug=p_slug
        )

        context = {}
        context["form"] = form
        context["workshop_slug"] = w_slug
        context["program_slug"] = p_slug
        context["back_urls"] = [
            (self.back_url(w_slug, p_slug), u"Назад к списку кураторов")
        ]

        me_curator = get_object_or_None(UserCuratorInfo, user=user)
        if me_curator is None:
            data_about_me = {
                "first_name": user.first_name,
                "last_name": user.last_name,
                "email": user.email,
            }
            uac = get_object_or_None(UserAdditionalContacts, user=user)
            if uac is not None:
                data_about_me["vk"] = uac.vk
                data_about_me["phone"] = uac.phone
            up = get_object_or_None(UserProfile, user=user)
            if up is not None:
                data_about_me["middle_name"] = up.middle_name
            context["me"] = data_about_me
        context["has_me"] = me_curator is not None

        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        user = get_user(request)
        w_slug, p_slug = kwargs["w_slug"], kwargs["p_slug"]

        w_metas = user.workshopprogrammetainfo_set.all()
        if not w_metas.filter(workshop_slug=w_slug, program_slug=p_slug).exists():
            messages.error(
                request, u"? Вы пытаетесь сделать что-то нехорошое. Не надо так"
            )
            return redirect("profile")

        context = {}
        context["workshop_slug"] = w_slug
        context["program_slug"] = p_slug

        context["back_urls"] = [
            (self.back_url(w_slug, p_slug), u"Назад к списку кураторов")
        ]

        form = self.form_class(request.POST, request.FILES)
        form.fields["workshop_program"].choices = [
            (wp.pk, u"%s: %s" % (wp.workshop_name, wp.program_name)) for wp in w_metas
        ]

        if "lectures" in form.data:
            context["lectures_data"] = json.loads(form.data["lectures"])

        if form.is_valid():
            cd = form.cleaned_data

            if cd["email"] != user.email:
                adapter = get_adapter()
                u = adapter.new_user(None)
                adapter.save_user(request, u, form, commit=True)

                password = generate_random_password()
                u.set_password(password)
                u.save()

                em = EmailAddress(
                    user=u, email=cd["email"], primary=True, verified=True
                )
                em.save()

                profile = get_object_or_None(UserProfile, user=u)
                if profile is None:
                    # FIXME is_male=False... Should opt to nullablefield in model
                    profile = UserProfile(user=u, is_male=False)
                    profile.middle_name = cd["middle_name"]
                    profile.is_curator = cd["apps_access"]
                    profile.save()

                    if cd["apps_access"]:
                        u.workshopprogrammetainfo_set.add(cd["workshop_program"])

                        subject = u"ЛШ2020: Добро пожаловать, ты куратор"
                        template = loader.get_template(
                            "intake/curators/emails/welcome_curator.txt"
                        )
                        text = template.render(
                            Context(
                                {
                                    "w_slug": w_slug,
                                    "p_slug": p_slug,
                                    "email": cd["email"],
                                    "password": password,
                                }
                            )
                        )

                        from_email = get_config(
                            "DEFAULT_FROM_EMAIL", "no-reply@letnyayashkola.ru"
                        )
                        if send_mail(subject, text, from_email, [cd["email"]]) != 1:
                            messages.error(
                                request,
                                u"Приветственный емейл отправить не получилось. Свяжись с куратором сам",
                            )
                        else:
                            messages.success(
                                request, u"Приветственный емейл отправлен!"
                            )

                else:
                    messages.error(request, u"Не могу создать профиль. Cry/Try again")

                uac = get_object_or_None(UserAdditionalContacts, user=u)
                if uac is None:
                    uac = UserAdditionalContacts(user=u, phone=cd["phone"], vk=cd["vk"])
                    uac.save()
                else:
                    messages.error(
                        request, u"Не могу сохранить контакты. Cry/Try again"
                    )
            else:
                u = user

            ph = cd.get("photo")
            if ph:
                uf, _ = UserPhoto.objects.get_or_create(user=u)
                uf.photo = ph
                uf.save()

            curator = form.save(commit=False)
            curator.user = u
            curator.save()

            messages.success(request, u"Добавлен куратор '%s'." % curator)
            return redirect("curators:curators_list", w_slug=w_slug, p_slug=p_slug)
        else:
            messages.error(request, u"Форма заполнена неполностью и/или с ошибками")

        context["form"] = form
        return render(request, self.template_name, context)


class CuratorCuratorDelete(CuratorRequiredMixin, generic.View):
    def back_url(self, w_slug, p_slug):
        return reverse(
            "curators:curators_list", kwargs={"w_slug": w_slug, "p_slug": p_slug}
        )

    def post(self, request, *args, **kwargs):
        w_slug = self.kwargs["w_slug"]
        p_slug = self.kwargs["p_slug"]
        user = get_user(self.request)

        back_url = self.back_url(w_slug, p_slug)
        success_url = back_url

        w_metas = user.workshopprogrammetainfo_set.all()
        if not w_metas.filter(workshop_slug=w_slug, program_slug=p_slug).exists():
            messages.error(
                request, u"? Вы пытаетесь сделать что-то нехорошое. Не надо так"
            )
            return redirect(back_url)

        curator = get_object_or_None(
            UserCuratorInfo,
            pk=self.kwargs["curator_pk"],
            workshop_program__workshop_slug=w_slug,
            workshop_program__program_slug=p_slug,
        )

        if curator is None:
            messages.error(
                request,
                u"? Или у вас не хватает прав для этого действия, или такого препода нет",
            )
            return redirect(back_url)

        if user != curator.user:
            curator.user.delete()
        else:
            curator.delete()
        messages.success(request, u"Куратор удалён")

        return redirect(success_url)


class CuratorCuratorsList(CuratorRequiredMixin, generic.ListView):
    template_name = "intake/curators/elders/curators_list.html"
    context_object_name = "curators"
    paginate_by = 10

    def get_queryset(self):
        w_slug = self.kwargs["w_slug"]
        p_slug = self.kwargs["p_slug"]
        u = get_user(self.request)

        if not u.userprofile.is_curator_for(w_slug, p_slug):
            return redirect("profile")

        qs = get_wp_curators(w_slug, p_slug)

        return qs

    def get_context_data(self, **kwargs):
        w_slug = self.kwargs["w_slug"]
        p_slug = self.kwargs["p_slug"]
        w_meta = get_object_or_404(
            WorkshopProgramMetaInfo, workshop_slug=w_slug, program_slug=p_slug
        )

        context = super(CuratorCuratorsList, self).get_context_data(**kwargs)

        u = get_user(self.request)
        prof = u.userprofile
        context["profile"] = prof
        context["workshop_name"] = w_meta.workshop_name
        context["program_name"] = w_meta.program_name
        context["workshop_slug"] = w_meta.workshop_slug
        context["program_slug"] = w_meta.program_slug
        context["wp_counts"] = get_wp_counts_dict(w_slug, p_slug)

        return context


class Stats(CuratorRequiredMixin, generic.View):
    template_name = "intake/curators/stats.html"

    def get(self, request, *args, **kwargs):
        u = get_user(request)
        context = {"workshop_programs": []}
        for wp in u.workshopprogrammetainfo_set.all():
            counts = get_wp_counts_dict(wp.workshop_slug, wp.program_slug)
            apps = get_wp_accepted_apps(wp.workshop_slug, wp.program_slug)
            context["workshop_programs"].append(
                (
                    wp.workshop_slug,
                    wp.program_slug,
                    wp.workshop_name,
                    wp.program_name,
                    counts,
                    apps,
                )
            )
        return render(request, self.template_name, context)


class AppCuratorCommentView(CuratorRequiredMixin, generic.View):
    def post(self, request, *args, **kwargs):
        u = get_user(request)
        back_url = request.POST.get("back_url", "profile")

        a_pk = request.POST.get("a_pk", None)
        if a_pk is None:
            messages.error(request, u"Заявку не удалось принять. Напишите Саре.")
            return redirect(back_url)

        app = get_object_or_404(ApplicationCase, pk=a_pk)
        w_slug, p_slug = app.wp_slug_tuple

        if not u.userprofile.is_curator_for_app(pk=a_pk):
            messages.error(
                request, u"Ты чё-то попутал, дружище. %s -- не из твоих" % app
            )
            messages.error(request, u"Может ты вообще не куратор?")
            return redirect(back_url)

        app.short_comment = request.POST.get("short_comment_hidden", "")
        app.save()

        return redirect(back_url)


class CuratorAppSearch(CuratorRequiredMixin, generic.ListView):
    template_name = "intake/curators/app_search.html"
    context_object_name = "applications"

    def get_queryset(self):
        u = get_user(self.request)
        users = []
        for wp in u.workshopprogrammetainfo_set.all():
            users += [
                p.user
                for p in WorkshopPetitionConnection.objects.filter(
                    workshop_slug=wp.workshop_slug, program_slug=wp.program_slug
                )
            ]
        my_applicants = ApplicationCase.objects.filter(user__in=users, is_closed=True)

        if self.request.GET.get("search_1", False):
            s1 = self.request.GET.get("search_1")
            return my_applicants.filter(pk=s1)

        substr = self.request.GET.get("search_42", "").split()
        if len(substr) == 3:
            q = Q(user__email__icontains=substr[-1][1:-1])
            q &= Q(user__last_name__icontains=substr[1])
            q &= Q(user__first_name__icontains=substr[0])
            qs = my_applicants.filter(q).order_by("created")[:10]
        else:
            substr = self.request.GET.get("search_42", "")
            if substr != "":
                if substr.find(u"@") != -1:
                    q = Q(user__email__icontains=substr)
                else:
                    q = Q(user__last_name__icontains=substr)
                    q |= Q(user__first_name__icontains=substr)
                qs = my_applicants.filter(q).order_by("created")[:10]
            else:
                qs = []
        return qs
