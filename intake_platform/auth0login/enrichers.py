from django.http import HttpResponseRedirect

from django.contrib.auth import get_user_model
from intake_platform.common.models import UserProfile
import pprint


def create_profile(strategy, *args, **kwargs):
    if not kwargs['is_new']:
        return None

    user = kwargs['user']

    profile = UserProfile(user=user)
    profile.save()

    return None
