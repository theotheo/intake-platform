from urllib import request
from typing import Dict

from jose import jwt
from social_core.backends.oauth import BaseOAuth2


class Auth0(BaseOAuth2):
    """Auth0 OAuth authentication backend"""

    SCOPE_SEPARATOR = ' '
    ACCESS_TOKEN_METHOD = 'POST'
    REDIRECT_STATE = False
    EXTRA_DATA = [('picture', 'picture'), ('email', 'email'), ('gender', 'gender')]
    name = 'auth0'

    def authorization_url(self) -> str:
        return 'https://' + self.setting('DOMAIN') + '/authorize'

    def access_token_url(self) -> str:
        return 'https://' + self.setting('DOMAIN') + '/oauth/token'

    def get_user_id(self, details, response):
        """Return current user id."""
        return details['user_id']

    def get_user_details(self, response) -> Dict[str, str]:
        # Obtain JWT and the keys to validate the signature
        id_token = response.get('id_token')
        jwks = request.urlopen(
            'https://' + self.setting('DOMAIN') + '/.well-known/jwks.json'
        )
        issuer = 'https://' + self.setting('DOMAIN') + '/'
        audience = self.setting('KEY')  # CLIENT_ID
        payload = jwt.decode(
            id_token,
            jwks.read(),
            algorithms=['RS256'],
            audience=audience,
            issuer=issuer,
        )

        details = {
            'username': payload['nickname'],
            'first_name': payload['name'],
            'picture': payload['picture'],
            'user_id': payload['sub'],
        }

        if 'gender' in payload:
            details['gender'] = payload['gender']
        if 'email' in payload:
            details['email'] = payload['email']

        return details
