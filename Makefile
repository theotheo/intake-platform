PORT?=8000

init_env:
	./dev/create-dotenv.sh
	docker-compose -p intake_platform -f dev/docker-compose.yml up -d
	sleep 10
	poetry run ./manage.py migrate
	./dev/create-superuser.sh

start_env:
	docker-compose -p intake_platform -f dev/docker-compose.yml start

stop_env:
	docker-compose -p intake_platform -f dev/docker-compose.yml stop

django-run:
	set -a && \
	    poetry run ./manage.py runserver ${PORT}

django-shell:
	set -a && \
	    poetry run ./manage.py shell

django-applymigrations:
	set -a && \
	    poetry run ./manage.py migrate

django-dbshell:
	set -a && \
	    poetry run ./manage.py dbshell

poetry-shell:
	set -a && \
	    poetry shell
