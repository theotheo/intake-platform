#!/bin/sh

script="
from django.contrib.auth import get_user_model;

get_user_model().objects.create_superuser('admin', 'admin@admin.com', 'admin')
"

echo $script | poetry run ./manage.py shell
